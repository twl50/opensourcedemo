/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.nostra13.example.universalimageloader;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 * ������
 */
public final class Constants {

	// һ��ͼƬ����
	public static final String[] IMAGES = new String[] {
			// Heavy images
			"http://img5.imgtn.bdimg.com/it/u=2516858777,100441176&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=703851384,2731595237&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=1599470528,845169958&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=931767507,3471271844&fm=23&gp=0.jpg",
			"http://img4.imgtn.bdimg.com/it/u=2956008787,1787762149&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=2176959642,1752795571&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=1011249507,1436127417&fm=23&gp=0.jpg",
			"http://img2.imgtn.bdimg.com/it/u=5578391,693348873&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=1708951864,3388449507&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=2712457830,1073958095&fm=23&gp=0.jpg",
			"http://img2.imgtn.bdimg.com/it/u=873128708,2915292684&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=4011997012,1907854985&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=466774335,2987572506&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=3461819192,538530183&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=660250093,2558694506&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=2468982627,2477902966&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=3183314237,3407734612&fm=23&gp=0.jpg",
			"http://img4.imgtn.bdimg.com/it/u=438980956,2411361732&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=1636375080,1687107869&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=750781768,751398254&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=2835832855,3961287651&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=660250093,2558694506&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=3063583273,2642318980&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=133227865,960379446&fm=23&gp=0.jpg",
			"http://img2.imgtn.bdimg.com/it/u=2309001816,3685418214&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=1344332954,3578785184&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=3374273149,2956998586&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=3094725785,291943099&fm=23&gp=0.jpg",
			"http://img2.imgtn.bdimg.com/it/u=3313314567,3255984861&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=1117897327,1971393971&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=1727705630,2298330817&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=3572665622,331156705&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=2581353473,2179599668&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=754466694,500732627&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=905357920,3137771636&fm=23&gp=0.jpg",
			"http://img2.imgtn.bdimg.com/it/u=2988391480,953194528&fm=23&gp=0.jpg",
			"http://img2.imgtn.bdimg.com/it/u=3752343722,205713720&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=3626319552,2059659031&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=1573627054,3409855583&fm=23&gp=0.jpg",
			// Light images
			"http://img1.imgtn.bdimg.com/it/u=989750496,2565087073&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=1638602722,2062619241&fm=23&gp=0.jpg",
			"http://img4.imgtn.bdimg.com/it/u=199834320,4170526508&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=2382870089,3669008052&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=1919009956,136281985&fm=23&gp=0.jpg",
			"http://img2.imgtn.bdimg.com/it/u=2288299488,2483877198&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=3109774322,3741869930&fm=23&gp=0.jpg",
			"http://img4.imgtn.bdimg.com/it/u=343741953,3405876889&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=637371208,2985325901&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=1593273701,1795157836&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=403974407,2466270446&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=3566887443,3610818248&fm=23&gp=0.jpg",
			"http://img4.imgtn.bdimg.com/it/u=679965454,2589185112&fm=23&gp=0.jpg",
			"http://img2.imgtn.bdimg.com/it/u=567463925,2089400373&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=1372843233,3033391783&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=2876095872,40157805&fm=23&gp=0.jpg",
			"http://img4.imgtn.bdimg.com/it/u=3958716955,4196020639&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=2159005352,3904718329&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=275764593,3505321054&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=551200499,4000073738&fm=23&gp=0.jpg",
			"http://img0.imgtn.bdimg.com/it/u=3374147333,743918965&fm=23&gp=0.jpg",
			"http://img3.imgtn.bdimg.com/it/u=2350163715,3530254444&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=2577978965,2146402528&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=1889177767,3393601448&fm=23&gp=0.jpg",
			"http://img5.imgtn.bdimg.com/it/u=3610157928,2675249901&fm=23&gp=0.jpg",
			"http://img1.imgtn.bdimg.com/it/u=104625927,3382044712&fm=23&gp=0.jpg",
			// Special cases
			"http://cdn.urbanislandz.com/wp-content/uploads/2011/10/MMSposter-large.jpg", // very large image
			"file:///sdcard/Universal Image Loader @#&=+-_.,!()~'%20.png", // Image from SD card with encoded symbols
			"assets://Living Things @#&=+-_.,!()~'%20.jpg", // Image from assets
			"drawable://" + R.drawable.ic_launcher, // Image from drawables
			"http://upload.wikimedia.org/wikipedia/ru/b/b6/Как_кот_с_мышами_воевал.png", // Link with UTF-8
			"https://www.eff.org/sites/default/files/chrome150_0.jpg", // Image from HTTPS
			"http://bit.ly/soBiXr", // Redirect link
			"http://img001.us.expono.com/100001/100001-1bc30-2d736f_m.jpg", // EXIF
			"", // Empty link
			"http://wrong.site.com/corruptedLink", // Wrong link
	};

	private Constants() {
	}

	// ����
	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}
	
	// ������
	public static class Extra {
		public static final String IMAGES = "com.nostra13.example.universalimageloader.IMAGES";
		public static final String IMAGE_POSITION = "com.nostra13.example.universalimageloader.IMAGE_POSITION";
	}
}
