package com.baofeng.mj.market.game.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;

import com.handmark.pulltorefresh.library.OverscrollHelper;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

/**
 * Created by hanyang on 2015/12/24.
 */
public class PullToRefreshRecycle extends PullToRefreshBase<RecyclerView> {
    public PullToRefreshRecycle(Context context) {
        super(context);
    }

    public PullToRefreshRecycle(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    public Orientation getPullToRefreshScrollDirection() {
        return Orientation.VERTICAL;
    }

    @Override
    protected RecyclerView createRefreshableView(Context context, AttributeSet attributeSet) {
        RecyclerView viewPager = new InternalScrollViewSDK9(context, attributeSet);
        return viewPager;
    }

    @Override
    protected boolean isReadyForPullEnd() {
        RecyclerView refreshableView = getRefreshableView();
        Adapter adapter = refreshableView.getAdapter();
        if (null == adapter) {
            return true;
        }
        return isBottom(refreshableView);
    }

    @Override
    protected boolean isReadyForPullStart() {
        RecyclerView refreshableView = getRefreshableView();
        Adapter adapter = refreshableView.getAdapter();
        if (null == adapter) {
            return true;
        }
        return isTop(refreshableView);
    }

    /**
     * 是否recyclerView的底部
     */
    private boolean isBottom(RecyclerView recyclerView) {
        LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (null == manager) {
            return true;
        } else {
            return manager.findLastCompletelyVisibleItemPosition() == recyclerView.getAdapter().getItemCount() - 1;
        }
    }

    /**
     * 是否recyclerView的顶部
     */
    private boolean isTop(RecyclerView recyclerView) {
        LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (null == manager) {
            return true;
        } else {
            return recyclerView.getAdapter().getItemCount() == 0 || manager.findFirstCompletelyVisibleItemPosition() == 0;
        }
    }

    final class InternalScrollViewSDK9 extends RecyclerView {

        public InternalScrollViewSDK9(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
            final boolean returnValue = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);
            OverscrollHelper.overScrollBy(PullToRefreshRecycle.this, deltaX,
                    scrollX, deltaY, scrollY, getScrollRange(), isTouchEvent);

            return returnValue;
        }

        /**
         * Taken from AOSP Scroll source
         */
        private int getScrollRange() {
            int scrollRange = 0;
            if (getChildCount() > 0) {
                View child = getChildAt(0);
                scrollRange = Math.max(0, child.getHeight() - (getHeight() - getPaddingBottom() - getPaddingTop()));
            }
            return scrollRange;
        }
    }
}
