package com.baofeng.mj.market.game.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baofeng.mj.market.game.R;
import com.baofeng.mj.market.game.activity.GameMainFragmentActivity;
import com.baofeng.mj.market.game.business.GameDownLoadBusiness;
import com.baofeng.mj.market.game.enity.GameAdBean;
import com.baofeng.mj.market.game.enity.GameAdBindInfo;
import com.baofeng.mj.market.game.enity.GameChannelBean;
import com.baofeng.mj.market.game.enity.GameNextListBean;
import com.baofeng.mj.market.game.enity.GameRecommendBean;
import com.baofeng.mj.market.game.enity.GameTagBean;
import com.baofeng.mj.market.game.enity.GameWelfareBean;
import com.baofeng.mj.market.game.enity.GameWelfarePicBean;
import com.baofeng.mj.market.game.utils.GameChannelUtil;
import com.baofeng.mj.market.game.utils.GameUtil;
import com.baofeng.mj.market.game.utils.ImageLoaderUtils;
import com.baofeng.mj.market.game.utils.VrReportUtil;
import com.baofeng.mj.market.game.widget.GameAdView;
import com.baofeng.mj.market.game.widget.GameDialog;
import com.baofeng.mj.pubblico.common.NewVrUrlConfig;
import com.baofeng.mj.pubblico.util.DensityUtil;
import com.baofeng.mj.pubblico.util.NetworkUtil;
import com.baofeng.mj.pubblico.util.OkHttpUtil;
import com.baofeng.mjgl.panorama.entity.GameBaseBean;
import com.baofeng.mjgl.panorama.entity.GameResourceBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Request;


/**
 * 游戏adapter
 */
public class GameMainAdapter extends Adapter<RecyclerView.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {
    private Context context;

    private ArrayList<GameBaseBean> gameList;
    private LayoutInflater layoutInflater;
    private int screenWidth;
    private float SCALE_ONE_IMAGE = (16f / 9f);
    private float SCALE_TWO_IMAGE = (4f / 3f);
    private float WELFARE_LEFT_IMAGE = (87f / 125f);//福利三张图左边视图宽高比例
    private float WELFARE_RIGHT_IMAGE = (666f / 245f);//福利三张图左边试图宽高比例
    private float WELFARE_LEFT_IMAGE_WIDTH = (58f / 169f);//福利三张图左边视图所占宽度比例
    private float WELFARE_RIGHT_IMAGE_WIDTH = (111f / 169f);//福利三张图左边视图所占宽度比例
    private int lrSize = 10;//左右边距
    private GameDownLoadBusiness gameDownLoadBusiness;//下载相关业务
    private ImageLoaderUtils imageLoaderUtils;
    private String pagetype;
    private Activity thisactivity;

    public GameMainAdapter(Context context, ArrayList<GameBaseBean> gameList, GameDownLoadBusiness gameDownLoadBusiness, String pagetype, Activity activity) {
        this.context = context;
        this.gameList = gameList;
        this.gameDownLoadBusiness = gameDownLoadBusiness;
        this.pagetype = pagetype;
        this.thisactivity = activity;
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(context);
        }
        screenWidth = GameUtil.getScreenWidth((Activity) context);
        imageLoaderUtils = ImageLoaderUtils.getInstance(context);
    }

    @Override
    public int getItemCount() {
        if (gameList != null) {
            return gameList.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (gameList != null && position < gameList.size()) {
            return gameList.get(position).getType();
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType, int i1) {
        switch (viewType) {
            case GameChannelUtil.GAME_SPECIL:
                View specialView = layoutInflater.inflate(R.layout.res_game_main_type_one_image_layout, null);
                return new SpecialViewHolder(specialView);
            case GameChannelUtil.GAME_TWO:
                View twoView = layoutInflater.inflate(R.layout.res_game_main_type_two_image_layout, null);
                return new TwoViewHolder(twoView);
            case GameChannelUtil.GAME_THREE:
                View threeView = layoutInflater.inflate(R.layout.res_game_main_type_three_image_layout, null);
                return new ThreeViewHolder(threeView);
            case GameChannelUtil.GAME_LIST:
                View listView = layoutInflater.inflate(R.layout.res_game_main_type_list_with_des, null);
                return new ListViewHolder(listView);
            case GameChannelUtil.GAME_WELFARE:
                View welThreeView = layoutInflater.inflate(R.layout.res_game_welfare_second_item_layout, null);
                return new WelThreeViewHolder(welThreeView);
            case GameChannelUtil.GAME_SPECIL_AD:
                GameAdView special_AdView = new GameAdView(context, pagetype);
                return new SpecialAdViewHolder(special_AdView);
            case GameChannelUtil.GAME_FOUR_IMG:
                View fourImg_view = layoutInflater.inflate(R.layout.res_game_main_type_four_layout, null);
                return new FourImgViewHolder(fourImg_view);
            case GameChannelUtil.GAME_RECONNEND:
                View recView = layoutInflater.inflate(R.layout.res_game_main_type_six_item_layout, null);
                return new GameRecViewHolder(recView);
            case GameChannelUtil.GAME_MULT_IMG:
                View mulImg = layoutInflater.inflate(R.layout.res_game_main_type_item_layout, null);
                return new MulImgViewHolder(mulImg);
            case GameChannelUtil.GAME_TAG:
                View tagView = layoutInflater.inflate(R.layout.res_game_main_tag_layout, null);
                return new GameTagViewHolder(tagView);
            case GameChannelUtil.GAME_NEXT_LIST:
                View nextlistView = layoutInflater.inflate(R.layout.res_game_next_list_btn, null);
                return new GameNextListViewHolder(nextlistView);
            default:
                View defaultview = layoutInflater.inflate(R.layout.res_game_main_type_default_layout, null);
                return new DefaultViewHolder(defaultview);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        GameBaseBean gameBeanBase = gameList.get(position);
        viewHolder.itemView.setTag(gameBeanBase);
        switch (getItemViewType(position)) {
            case GameChannelUtil.GAME_SPECIL:
                fillSpecilView((SpecialViewHolder) viewHolder, (GameChannelBean) gameBeanBase, position);
                break;
            case GameChannelUtil.GAME_TWO:
                fillTwoView((TwoViewHolder) viewHolder, (GameChannelBean) gameBeanBase);
                break;
            case GameChannelUtil.GAME_THREE:
                fillThreeView((ThreeViewHolder) viewHolder, (GameResourceBean) gameBeanBase);
                break;
            case GameChannelUtil.GAME_LIST:
                fillListView((ListViewHolder) viewHolder, (GameResourceBean) gameBeanBase);
                break;
            case GameChannelUtil.GAME_WELFARE:
                fillWelfareThreeView((WelThreeViewHolder) viewHolder, (GameWelfareBean) gameBeanBase);
                break;
            case GameChannelUtil.GAME_SPECIL_AD:
                fillSpecialAdView((SpecialAdViewHolder) viewHolder, (GameAdBean) gameBeanBase);
                break;
            case GameChannelUtil.GAME_FOUR_IMG:
                fillFourImgView((FourImgViewHolder) viewHolder, (GameResourceBean) gameBeanBase);
                break;
            case GameChannelUtil.GAME_MULT_IMG:
                fillMulImg((MulImgViewHolder) viewHolder, (GameChannelBean) gameBeanBase, position);
                break;
            case GameChannelUtil.GAME_RECONNEND:
                fillRecView((GameRecViewHolder) viewHolder, (GameRecommendBean) gameBeanBase, position);
                break;
            case GameChannelUtil.GAME_TAG:
                fillGameTagView((GameTagViewHolder) viewHolder, (GameTagBean) gameBeanBase);
                break;
            case GameChannelUtil.GAME_NEXT_LIST:
                fillNextListView((GameNextListViewHolder) viewHolder, (GameNextListBean) gameBeanBase, position);
                break;
            default:
                fillDefaultView();
                break;
        }
    }

    //填充专题视图
    private void fillSpecilView(SpecialViewHolder viewHolder, GameChannelBean gameChannelBean, final int position) {
        //判断资源类型，是否显示游戏专题标签
        if (gameChannelBean.getDirect_type() == 1) {//单个游戏
            viewHolder.topic_tag.setVisibility(View.GONE);
        } else if (gameChannelBean.getDirect_type() == 5) {//专题列表
            viewHolder.topic_tag.setVisibility(View.VISIBLE);
        } else {
            viewHolder.topic_tag.setVisibility(View.GONE);
        }
        imageLoaderUtils.getImageLoader().displayImage(gameChannelBean.getImg_url(), viewHolder.imageView, imageLoaderUtils.getImgOptions_16_9());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameChannelBean gameChannelBean = (GameChannelBean) v.getTag();
                int directType = gameChannelBean.getDirect_type();
                String pageTyp = pagetype;
                if (position >= 1 && position - 1 < gameList.size() && gameList.get(position - 1) instanceof GameTagBean) {
                    if (((GameTagBean) gameList.get(position - 1)).getTag().equals("热点")) {
                        pageTyp = "u_prefer_hot";
                    } else {
                        pageTyp = pagetype;
                    }
                }
                if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                    VrReportUtil.reportVrGameClickForBoon(pageTyp, "jump", directType, gameChannelBean.getBaoshu_id(), gameChannelBean.getImg_name());
                } else {//不是福利页
                    VrReportUtil.reportVrGameClick(pageTyp, "jump", gameChannelBean.getBaoshu_id(), directType,gameChannelBean.getImg_name());
                }
                if (directType == 1) {//单个游戏详情
                    GameUtil.gotoGameDetailByDirectResource(context, gameChannelBean.getDirect_resource());
                } else if (directType == 2 || directType == 5) {// 跳转到专题页面
                    GameUtil.gotoGameDSpecialByDirectResource(context, gameChannelBean.getDirect_resource(), gameChannelBean.getDirect_type());
                } else if (directType == 4) {//跳转链接地址
                    GameUtil.gotoWebView(context, gameChannelBean.getDirect_resource());
                }
            }
        });
    }

    //填充2x1视图
    private void fillTwoView(TwoViewHolder viewHolder, GameChannelBean gameChannelBean) {
        if (gameChannelBean.getDirect_type() == 1) {//单个游戏
            viewHolder.topic_tag.setVisibility(View.GONE);
        } else if (gameChannelBean.getDirect_type() == 5) {//专题列表
            viewHolder.topic_tag.setVisibility(View.VISIBLE);
        } else {//其他
            viewHolder.topic_tag.setVisibility(View.GONE);
        }
        int imageWidth = (screenWidth - (DensityUtil.getPx(context, lrSize * 2
                + lrSize))) / 2;
        int imageHeight = (int) (imageWidth / SCALE_TWO_IMAGE);
        android.widget.LinearLayout.LayoutParams layoutParams = new android.widget.LinearLayout.LayoutParams(imageWidth, imageHeight);
        int image_Margin = DensityUtil.getPx(context, 10);
        if (gameChannelBean.getViewPosition() % 2 != 0) {
            layoutParams.setMargins(0, 0, image_Margin / 2, 0);
        }
        if (gameChannelBean.getViewPosition() % 2 == 0) {
            layoutParams.setMargins(image_Margin / 2, 0, 0, 0);
        }
        viewHolder.ly_imageview.setLayoutParams(layoutParams);
        imageLoaderUtils.getImageLoader().displayImage(gameChannelBean.getImg_url(), viewHolder.imageView, imageLoaderUtils.getImgOptions_16_9());
    }

    //填充3x1视图
    private void fillThreeView(ThreeViewHolder viewHolder, GameResourceBean gameResourceBean) {
        if (gameResourceBean == null) {
            return;
        }
        int imageWidth = (screenWidth - (DensityUtil.getPx(context, lrSize * 4))) / 6;
        int imageHeight = imageWidth;
        android.widget.LinearLayout.LayoutParams layoutParamsImage = new android.widget.LinearLayout.LayoutParams(imageWidth, imageHeight);
        viewHolder.imageView.setLayoutParams(layoutParamsImage);
        int itemWidth = (screenWidth - (DensityUtil.getPx(context, lrSize * 4))) / 3;
        android.widget.LinearLayout.LayoutParams layoutParams = new android.widget.LinearLayout.LayoutParams(itemWidth, -1);
        switch (gameResourceBean.getViewPosition()) {
            case 1:
                layoutParams.gravity = Gravity.CENTER | Gravity.RIGHT;
                break;
            case 2:
                layoutParams.gravity = Gravity.CENTER;
                break;
            case 3:
                layoutParams.gravity = Gravity.CENTER | Gravity.LEFT;
                break;
            default:
                break;
        }
        viewHolder.ly_newvr_game_three_top.setLayoutParams(layoutParams);
        viewHolder.tv_title.setText(gameResourceBean.getTitle_chs());
        viewHolder.tv_type_name.setText(gameResourceBean.getTypename());
        imageLoaderUtils.getImageLoader().displayImage(gameResourceBean.getIcon_url(), viewHolder.imageView, imageLoaderUtils.getImgOptions_3_4());
        viewHolder.btn_download.setTag(gameResourceBean);
        gameDownLoadBusiness.add(viewHolder.btn_download);
    }

    //填充列表数据
    private void fillListView(ListViewHolder viewHolder, GameResourceBean gameBeanResource) {
        viewHolder.tv_title.setText(gameBeanResource.getTitle_chs());
        viewHolder.tv_count.setText(GameUtil.formatCount(gameBeanResource.getInstall_num()) + "次");
        GameUtil.setListTag(context, gameBeanResource, viewHolder.play_mode1, viewHolder.game_partner, viewHolder.cat_two_name, viewHolder.short_desc);
        viewHolder.tv_file_size.setText(gameBeanResource.getApp_size());
        viewHolder.btn_download.setTag(gameBeanResource);
        gameDownLoadBusiness.add(viewHolder.btn_download);
        imageLoaderUtils.getImageLoader().displayImage(gameBeanResource.getIcon_url(), viewHolder.imageView, imageLoaderUtils.getImgOptions_3_4());
    }

    /**
     * 专题ViewHolder
     */
    public class SpecialViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView topic_tag;

        public SpecialViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageview);
            topic_tag = (TextView) view.findViewById(R.id.topic_tag);
            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            layoutParams.width = (screenWidth - DensityUtil.getPx(context, 20));
            layoutParams.height = (int) ((screenWidth - DensityUtil.getPx(context, 20)) / SCALE_ONE_IMAGE);
            imageView.setLayoutParams(layoutParams);
        }
    }

    /**
     * 2x1ViewHolder
     */
    public class TwoViewHolder extends RecyclerView.ViewHolder {
        private View ly_imageview;
        private ImageView imageView;
        private TextView topic_tag;

        public TwoViewHolder(View view) {
            super(view);
            ly_imageview = view.findViewById(R.id.ly_imageview);
            imageView = (ImageView) view.findViewById(R.id.imageview);
            topic_tag = (TextView) view.findViewById(R.id.topic_tag);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GameChannelBean gameChannelBean = (GameChannelBean) v.getTag();
                    int directType = gameChannelBean.getDirect_type();
                    if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                        VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", directType, gameChannelBean.getBaoshu_id(), gameChannelBean.getImg_name());
                    } else {//不是福利页
                        VrReportUtil.reportVrGameClick(pagetype, "jump", gameChannelBean.getBaoshu_id(),directType, gameChannelBean.getImg_name());
                    }
                    if (directType == 1) {//游戏
                        GameUtil.gotoGameDetail(context, gameChannelBean.getBaoshu_id());
                    } else if (directType == 2 || directType == 5) {//专题
                        GameUtil.gotoGameDSpecialByDirectResource(context, gameChannelBean.getDirect_resource(), gameChannelBean.getDirect_type());
                    } else if (directType == 4) {//跳转链接地址
                        GameUtil.gotoWebView(context, gameChannelBean.getDirect_resource());
                    }
                }
            });
        }
    }

    /**
     * 3x1ViewHolder
     */
    public class ThreeViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private View ly_newvr_game_three_top;
        private TextView tv_title;
        private TextView tv_type_name;
        private Button btn_download;

        public ThreeViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageview);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_type_name = (TextView) view.findViewById(R.id.tv_type_name);
            btn_download = (Button) view.findViewById(R.id.btn_download);
            ly_newvr_game_three_top = view.findViewById(R.id.ly_newvr_game_three_top);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GameResourceBean gameResourceBean = (GameResourceBean) v.getTag();
                    GameUtil.gotoGameDetail(context, gameResourceBean.getAppid());
                    if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                        VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", 1, gameResourceBean.getAppid(), gameResourceBean.getTitle_chs());
                    } else {//不是福利页
                        VrReportUtil.reportVrGameClick(pagetype, "jump", gameResourceBean.getAppid(), 1, gameResourceBean
                                .getTitle_chs());
                    }
                }
            });
        }
    }

    /**
     * 资源列表ViewHolder
     */
    public class ListViewHolder extends RecyclerView.ViewHolder {
        private View iv_line;
        private ImageView imageView;
        private TextView tv_title;
        private TextView tv_count;
        private TextView tv_file_size, short_desc, cat_two_name, game_partner, play_mode1;
        private Button btn_download;

        public ListViewHolder(View view) {
            super(view);
            iv_line = view.findViewById(R.id.iv_line);
            imageView = (ImageView) view.findViewById(R.id.imageview);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_count = (TextView) view.findViewById(R.id.tv_count);
            tv_file_size = (TextView) view.findViewById(R.id.tv_file_size);
            short_desc = (TextView) view.findViewById(R.id.short_desc);
            cat_two_name = (TextView) view.findViewById(R.id.cat_two_name);
            game_partner = (TextView) view.findViewById(R.id.game_partner);
            play_mode1 = (TextView) view.findViewById(R.id.play_mode1);
            btn_download = (Button) view.findViewById(R.id.btn_download);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GameUtil.gotoGameDetail(context, ((GameResourceBean) v.getTag()).getAppid());
                    if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                        VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", 1, ((GameResourceBean) v.getTag()).getAppid(), ((GameResourceBean) v.getTag()).getTitle_chs());
                    } else {//不是福利页
                        VrReportUtil.reportVrGameClick(pagetype, "jump", ((GameResourceBean) v.getTag()).getAppid(), 1, ((GameResourceBean) v.getTag()).getTitle_chs());
                    }
                }
            });
        }
    }

    /**
     * 福利三张图ViewHolder
     */
    public class WelThreeViewHolder extends RecyclerView.ViewHolder {
        private ImageView second_welfare_icon, second_welfare_first_icon, second_welfare_second_icon;
        private TextView topic_tag_1, topic_tag_2, topic_tag_3;

        public WelThreeViewHolder(View itemView) {
            super(itemView);
            second_welfare_icon = (ImageView) itemView.findViewById(R.id.second_welfare_icon);
            second_welfare_first_icon = (ImageView) itemView.findViewById(R.id.second_welfare_first_icon);
            second_welfare_second_icon = (ImageView) itemView.findViewById(R.id.second_welfare_second_icon);
            topic_tag_1 = (TextView) itemView.findViewById(R.id.topic_tag_1);
            topic_tag_2 = (TextView) itemView.findViewById(R.id.topic_tag_2);
            topic_tag_3 = (TextView) itemView.findViewById(R.id.topic_tag_3);
            //设置福利左边视图宽高
            ViewGroup.LayoutParams second_welfare_icon_params = second_welfare_icon.getLayoutParams();
            int second_welfare_icon_width = (int) ((screenWidth - DensityUtil.getPx(context, 30)) * WELFARE_LEFT_IMAGE_WIDTH);
            int second_welfare_icon_height = (int) (second_welfare_icon_width / WELFARE_LEFT_IMAGE);
            second_welfare_icon_params.width = second_welfare_icon_width;
            second_welfare_icon_params.height = second_welfare_icon_height;
            second_welfare_icon.setLayoutParams(second_welfare_icon_params);
            //设置福利右上边视图宽高
            ViewGroup.LayoutParams second_welfare_first_icon_params = second_welfare_first_icon.getLayoutParams();
            int second_welfare_right_icon_width = (int) ((screenWidth - DensityUtil.getPx(context, 30)) * WELFARE_RIGHT_IMAGE_WIDTH);
            int second_welfare_right_icon_height = (int) (second_welfare_right_icon_width / WELFARE_RIGHT_IMAGE);
            second_welfare_first_icon_params.width = second_welfare_right_icon_width;
            second_welfare_first_icon_params.height = second_welfare_right_icon_height + DensityUtil.getPx(context, 2);
            second_welfare_first_icon.setLayoutParams(second_welfare_first_icon_params);
            //设置福利右下边视图宽高
            ViewGroup.LayoutParams second_welfare_second_icon_params = second_welfare_second_icon.getLayoutParams();
            second_welfare_second_icon_params.width = second_welfare_right_icon_width;
            second_welfare_second_icon_params.height = second_welfare_right_icon_height + DensityUtil.getPx(context, 2);
            second_welfare_second_icon.setLayoutParams(second_welfare_second_icon_params);
        }
    }

    /**
     * 福利三张图填充数据
     *
     * @param viewHolder
     * @param gameWelfareBean
     */
    public void fillWelfareThreeView(WelThreeViewHolder viewHolder, final GameWelfareBean gameWelfareBean) {
        if (!TextUtils.isEmpty(gameWelfareBean.getData().toString()) && gameWelfareBean.getData().size() >= 3) {
            //控制专题tag显示
            if (gameWelfareBean.getData().get(0).getDirect_type() == 5) {
                viewHolder.topic_tag_1.setVisibility(View.VISIBLE);
            } else {
                viewHolder.topic_tag_1.setVisibility(View.GONE);
            }
            final GameWelfarePicBean gameWelfarePicBean = gameWelfareBean.getData().get(0);
            viewHolder.second_welfare_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int directType = gameWelfarePicBean.getDirect_type();
                    if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                        VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", directType, "", gameWelfarePicBean.getImg_name());
                    } else {//不是福利页
                        VrReportUtil.reportVrGameClick(pagetype, "jump", "", directType, gameWelfarePicBean.getImg_name());
                    }
                    if (directType == 1) {
                        GameUtil.gotoGameDetailByDirectResource(context, gameWelfarePicBean.getDirect_resource());
                    } else if (directType == 2 || directType == 5) {
                        GameUtil.gotoGameDSpecialByDirectResource(context, gameWelfarePicBean.getDirect_resource(), gameWelfarePicBean.getDirect_type());
                    } else if (directType == 4) {//跳转链接地址
                        GameUtil.gotoWebView(context, gameWelfarePicBean.getDirect_resource());
                    }
                }
            });
            if (gameWelfareBean.getData().get(1).getDirect_type() == 5) {
                viewHolder.topic_tag_2.setVisibility(View.VISIBLE);
            } else {
                viewHolder.topic_tag_2.setVisibility(View.GONE);
            }
            final GameWelfarePicBean gameWelfarePicBean1 = gameWelfareBean.getData().get(1);
            viewHolder.second_welfare_first_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int directType = gameWelfarePicBean1.getDirect_type();
                    if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                        VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", directType, "", gameWelfarePicBean1.getImg_name());
                    } else {//不是福利页
                        VrReportUtil.reportVrGameClick(pagetype, "jump", "", directType, gameWelfarePicBean1.getImg_name());
                    }
                    if (directType == 1) {
                        GameUtil.gotoGameDetailByDirectResource(context, gameWelfarePicBean1.getDirect_resource());
                    } else if (directType == 2 || directType == 5) {
                        GameUtil.gotoGameDSpecialByDirectResource(context, gameWelfarePicBean1.getDirect_resource(), gameWelfarePicBean1.getDirect_type());
                    } else if (directType == 4) {//跳转链接地址
                        GameUtil.gotoWebView(context, gameWelfarePicBean1.getDirect_resource());
                    }
                }
            });
            if (gameWelfareBean.getData().get(2).getDirect_type() == 5) {
                viewHolder.topic_tag_3.setVisibility(View.VISIBLE);
            } else {
                viewHolder.topic_tag_3.setVisibility(View.GONE);
            }
            final GameWelfarePicBean gameWelfarePicBean2 = gameWelfareBean.getData().get(2);
            viewHolder.second_welfare_second_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int directType = gameWelfarePicBean2.getDirect_type();
                    if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                        VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", directType, "", gameWelfarePicBean2.getImg_name());
                    } else {//不是福利页
                        VrReportUtil.reportVrGameClick(pagetype, "jump", "", directType, gameWelfarePicBean2.getImg_name());
                    }
                    if (directType == 1) {
                        GameUtil.gotoGameDetailByDirectResource(context, gameWelfarePicBean2.getDirect_resource());
                    } else if (directType == 2 || directType == 5) {
                        GameUtil.gotoGameDSpecialByDirectResource(context, gameWelfarePicBean2.getDirect_resource(), gameWelfarePicBean2.getDirect_type());
                    } else if (directType == 4) {//跳转链接地址
                        GameUtil.gotoWebView(context, gameWelfarePicBean2.getDirect_resource());
                    }
                }
            });
            imageLoaderUtils.getImageLoader().displayImage(gameWelfareBean.getData().get(0).getImg_url(), viewHolder.second_welfare_icon, imageLoaderUtils.getImgOptions_game_welfare_second_left_bg());
            imageLoaderUtils.getImageLoader().displayImage(gameWelfareBean.getData().get(1).getImg_url(), viewHolder.second_welfare_first_icon, imageLoaderUtils.getImgOptions_game_welfare_second_right_bg());
            imageLoaderUtils.getImageLoader().displayImage(gameWelfareBean.getData().get(2).getImg_url(), viewHolder.second_welfare_second_icon, imageLoaderUtils.getImgOptions_game_welfare_second_right_bg());
        }
    }

    /**
     * 轮播广告位
     */
    public class SpecialAdViewHolder extends RecyclerView.ViewHolder {
        public SpecialAdViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * 福利单张视图
     *
     * @param viewHolder
     * @param gameAdBean
     */
    public void fillSpecialAdView(final SpecialAdViewHolder viewHolder, final GameAdBean gameAdBean) {
        ((GameAdBindInfo) viewHolder.itemView).bindData(gameAdBean);
    }

    /**
     * 填充多图数据
     *
     * @param viewHolder
     * @param gameChannelBean
     */
    public void fillMulImg(MulImgViewHolder viewHolder, final GameChannelBean gameChannelBean, int position) {
        imageLoaderUtils.getImageLoader().displayImage(gameChannelBean.getImg_url(), viewHolder.game_type_img, imageLoaderUtils.getImgOptions_game_type_bg());
        viewHolder.game_type_right.setText(gameChannelBean.getImg_name());
        int width = (int) ((screenWidth / 2) + DensityUtil.getPx(context, 30));
        int height = (int) ((width * 165f) / 472f);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
        if (gameChannelBean.getViewPosition() % 2 == 0) {
            layoutParams.setMargins(DensityUtil.getPx(context, 8), 0, 0, DensityUtil.getPx(context, 10));
        } else {
            layoutParams.setMargins(0, 0, DensityUtil.getPx(context, 8), DensityUtil.getPx(context, 10));
        }
        viewHolder.itemView.setLayoutParams(layoutParams);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int directType = gameChannelBean.getDirect_type();
                if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                    VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", directType, gameChannelBean.getBaoshu_id(), gameChannelBean.getImg_name());
                } else {//不是福利页
                    VrReportUtil.reportVrGameClick(pagetype, "jump", gameChannelBean.getBaoshu_id(), directType, gameChannelBean.getImg_name());
                }
                if (directType == 1) {
                    GameUtil.gotoGameDetailByDirectResource(context, gameChannelBean.getDirect_resource());
                } else if (directType == 2 || directType == 5) {
                    GameUtil.gotoGameDSpecialByDirectResource(context, gameChannelBean.getDirect_resource(), gameChannelBean.getDirect_type());
                } else if (directType == 4) {//跳转链接地址
                    GameUtil.gotoWebView(context, gameChannelBean.getDirect_resource());
                }
            }
        });
    }

    /**
     * 多图
     */
    public class MulImgViewHolder extends RecyclerView.ViewHolder {
        private ImageView game_type_img;
        private TextView game_type_right;

        public MulImgViewHolder(View itemView) {
            super(itemView);
            game_type_img = (ImageView) itemView.findViewById(R.id.game_type_img);
            game_type_right = (TextView) itemView.findViewById(R.id.game_type_right);
        }
    }

    /**
     * 4x1
     *
     * @param viewHolder
     * @param gameResourceBean
     */
    public void fillFourImgView(FourImgViewHolder viewHolder, final GameResourceBean gameResourceBean) {
        imageLoaderUtils.getImageLoader().displayImage(gameResourceBean.getIcon_url(), viewHolder.game_main_type_four_icon, imageLoaderUtils.getImgOptions_3_4());
        viewHolder.game_four_type_title.setText(gameResourceBean.getTitle_chs());
        viewHolder.btn_download.setTag(gameResourceBean);
        gameDownLoadBusiness.add(viewHolder.btn_download);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                    VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", 1, gameResourceBean.getAppid(), gameResourceBean.getTitle_chs());
                } else {//不是福利页
                    VrReportUtil.reportVrGameClick(pagetype, "jump", gameResourceBean.getAppid(), 1, gameResourceBean.getTitle_chs());
                }
                GameUtil.gotoGameDetail(context, gameResourceBean.getAppid());
            }
        });
    }

    /**
     * 4x1
     */
    public class FourImgViewHolder extends RecyclerView.ViewHolder {
        private ImageView game_main_type_four_icon;
        private TextView game_four_type_title;
        private Button btn_download;

        public FourImgViewHolder(View itemView) {
            super(itemView);
            int imageWidth = (screenWidth - (DensityUtil.getPx(context, 17 * 4))) / 4;
            int imageHeight = imageWidth;
            android.widget.LinearLayout.LayoutParams layoutParamsImage = new android.widget.LinearLayout.LayoutParams(imageWidth, imageHeight);
            game_main_type_four_icon = (ImageView) itemView.findViewById(R.id.game_main_type_four_icon);
            game_main_type_four_icon.setLayoutParams(layoutParamsImage);
            game_four_type_title = (TextView) itemView.findViewById(R.id.game_four_type_title);
            btn_download = (Button) itemView.findViewById(R.id.btn_download);
        }
    }

    /**
     * 小编推荐
     */
    public class GameRecViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout six_layout_1, six_layout_2;
        private ImageView item_six_icon, item_six_icon_1;
        private TextView item_six_name, item_six_des;
        private ImageView iv_line;

        public GameRecViewHolder(View itemView) {
            super(itemView);
            six_layout_1 = (RelativeLayout) itemView.findViewById(R.id.six_layout_1);
            item_six_icon = (ImageView) itemView.findViewById(R.id.item_six_icon);
            item_six_name = (TextView) itemView.findViewById(R.id.item_six_name);
            item_six_des = (TextView) itemView.findViewById(R.id.item_six_des);
            iv_line = (ImageView) itemView.findViewById(R.id.iv_line);
        }
    }

    /**
     * 小编推荐模块数据填充
     *
     * @param gameRecViewHolder
     * @param gameRecommendBean
     */
    public void fillRecView(GameRecViewHolder gameRecViewHolder, final GameRecommendBean gameRecommendBean, int position) {
        int width = (int) ((screenWidth - DensityUtil.getPx(context, 20)) / 2);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (position - 1 >= 0 && (gameList.get(position - 1) instanceof GameTagBean || gameList.get(position - 2) instanceof GameTagBean)) {
            layoutParams.setMargins(0, 0, 0, 0);
        } else {
            layoutParams.setMargins(0, DensityUtil.getPx(context, 10), 0, 0);
        }
        gameRecViewHolder.six_layout_1.setLayoutParams(layoutParams);
        if (TextUtils.isEmpty(gameRecommendBean.toString())) {
            gameRecViewHolder.itemView.setVisibility(View.GONE);
            return;
        } else {
            gameRecViewHolder.itemView.setVisibility(View.VISIBLE);
        }
        if (gameRecommendBean.getPosition() == 5 || gameRecommendBean.getPosition() == 6) {
            gameRecViewHolder.iv_line.setVisibility(View.GONE);
        } else {
            gameRecViewHolder.iv_line.setVisibility(View.VISIBLE);
        }
        gameRecViewHolder.item_six_name.setText(gameRecommendBean.getTitle_chs());
        gameRecViewHolder.item_six_des.setText(gameRecommendBean.getRecommand_reason());
        gameRecViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pagetype.equals(GameMainFragmentActivity.PAGE_TYPE_BOON)) {//福利页
                    VrReportUtil.reportVrGameClickForBoon(pagetype, "jump", 1, gameRecommendBean.getId(), gameRecommendBean.getTitle_chs());
                } else {//不是福利页
                    VrReportUtil.reportVrGameClick(pagetype, "jump", gameRecommendBean.getId(), 1, gameRecommendBean.getTitle_chs());
                }
                GameUtil.gotoGameDetail(context, gameRecommendBean.getId());
            }
        });
        imageLoaderUtils.getImageLoader().displayImage(gameRecommendBean.getIcon_url(), gameRecViewHolder.item_six_icon, imageLoaderUtils.getImgOptions_3_4());
    }

    /**
     * 模块Tag标签
     */
    public class GameTagViewHolder extends RecyclerView.ViewHolder {
        private TextView game_tag, name, more;

        public GameTagViewHolder(View itemView) {
            super(itemView);
            game_tag = (TextView) itemView.findViewById(R.id.game_tag);
            name = (TextView) itemView.findViewById(R.id.name);
            more = (TextView) itemView.findViewById(R.id.more);
        }
    }

    /**
     * 填充模块tag
     *
     * @param viewHolder
     * @param gameTagBean
     */
    public void fillGameTagView(GameTagViewHolder viewHolder, final GameTagBean gameTagBean) {
        viewHolder.game_tag.setText(gameTagBean.getTag());
        viewHolder.name.setText(gameTagBean.getName());
        viewHolder.game_tag.setBackgroundResource(gameTagBean.getBgId());
        if (gameTagBean.getDirect_type() != 5) {
            viewHolder.more.setVisibility(View.GONE);
        } else {
            viewHolder.more.setVisibility(View.VISIBLE);
        }
        viewHolder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameTagBean.getDirect_type() == 5) {
                    if (!TextUtils.isEmpty(gameTagBean.getDirect_resource())) {
                        GameUtil.gotoGameDSpecialByDirectResource(context, gameTagBean.getDirect_resource(), gameTagBean.getDirect_type());
                    }
                }
            }
        });
    }

    /**
     * 换一批ViewHolder
     */
    public class GameNextListViewHolder extends RecyclerView.ViewHolder {
        private Button next_list;

        public GameNextListViewHolder(View itemView) {
            super(itemView);
            next_list = (Button) itemView.findViewById(R.id.next_list);
        }
    }

    /**
     * 换一批填充数据
     *
     * @param viewHolder
     * @param gameNextListBean
     */
    public void fillNextListView(final GameNextListViewHolder viewHolder, final GameNextListBean gameNextListBean, final int position) {
        viewHolder.next_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetworkUtil.isConnectInternet(context)) {
                    Toast.makeText(context, "当前网络错误请检查网络设置", Toast.LENGTH_SHORT).show();
                    return;
                }
                VrReportUtil.reportGameWelfareMainTitleClick("u_prefer_exch");
                if (gameNextListBean.isEnd()) {
                    gameNextListBean.setPageNum(1);
                } else {
                    gameNextListBean.setPageNum(gameNextListBean.getPageNum() + 1);
                }
                final String url = NewVrUrlConfig.getNextListUrl(gameNextListBean.getPageNum(), gameNextListBean.getVersion());
                if (!TextUtils.isEmpty(url)) {
                    GameDialog.showloadingDialog(thisactivity, null);
                    OkHttpUtil.getAsyn(url, new OkHttpUtil.ResultCallback<String>() {

                        @Override
                        public void onError(Request request, Exception e) {
                            GameDialog.cancelDialog();
                        }

                        @Override
                        public void onResponse(String response, String url) {
                            if (!TextUtils.isEmpty(response)) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (!jsonObject.isNull("data")) {
                                        int size = jsonObject.getJSONArray("data").length();
                                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                                        gameNextListBean.setEnd(jsonObject.getBoolean("end"));
                                        gameNextListBean.setPageNum(gameNextListBean.getPageNum());
                                        gameNextListBean.setType(99);
                                        gameNextListBean.setVersion(jsonObject.getString("version"));
                                        int len = size > 6 ? 6 : size;
                                        int n = 0;
                                        for (int i = 0; i < len; i++) {
                                            GameUtil.createGameRecommendBean((GameRecommendBean) gameList.get(position - 6 + i), jsonArray.optJSONObject(i), ++n);
                                        }
                                        GameDialog.cancelDialog();
                                        notifyItemRangeChanged(position - 6, position - 1);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                gameNextListBean.setEnd(true);
                                gameNextListBean.setPageNum(1);
                                gameNextListBean.setType(99);
                                gameNextListBean.setVersion(null);
                                GameDialog.cancelDialog();
                            }
                        }
                    });
                } else {
                    return;
                }
            }
        });

    }

    /**
     * 默认布局
     */
    public class DefaultViewHolder extends RecyclerView.ViewHolder {

        public DefaultViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * 填充默认布局
     */
    public void fillDefaultView() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    public void setGameList(ArrayList<GameBaseBean> gameList) {
        this.gameList = gameList;
    }
}
