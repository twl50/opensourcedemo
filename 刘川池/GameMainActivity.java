package com.baofeng.mj.market.game.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.baofeng.mj.market.game.R;
import com.baofeng.mj.market.game.adapter.GameMainAdapter;
import com.baofeng.mj.market.game.business.GameDownLoadBusiness;
import com.baofeng.mj.market.game.enity.GameNextListBean;
import com.baofeng.mj.market.game.enity.GameRecommendBean;
import com.baofeng.mj.market.game.enity.GameTagBean;
import com.baofeng.mj.market.game.enity.GameThreeBean;
import com.baofeng.mj.market.game.fragment.BaseGameFragment;
import com.baofeng.mj.market.game.utils.GameChannelUtil;
import com.baofeng.mj.market.game.utils.GameUtil;
import com.baofeng.mj.market.game.utils.SynchronizedDownload;
import com.baofeng.mj.market.game.utils.VrReportUtil;
import com.baofeng.mj.market.game.widget.PullToRefreshRecycle;
import com.baofeng.mj.pubblico.common.NewVrUrlConfig;
import com.baofeng.mj.pubblico.util.NetworkUtil;
import com.baofeng.mj.pubblico.util.OkHttpUtil;
import com.baofeng.mj.pubblico.util.StringUtil;
import com.baofeng.mjgl.panorama.entity.GameBaseBean;
import com.baofeng.mjgl.panorama.entity.GameResourceBean;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Request;


/**
 * Created by hanyang on 2015/12/23.
 */
public class GameMainActivity extends BaseGameFragment implements PullToRefreshBase.OnRefreshListener2<RecyclerView> {
    private String pagetype = null;
    private PullToRefreshRecycle pullToRefreshRecycle;
    private RecyclerView recyclerView;
    private LinearLayout ly_content, ly_error;
    private GridLayoutManager gridLayoutManager;
    private static final int SPAN_COUNT = 12;//列数
    private ArrayList<GameBaseBean> dataList = new ArrayList<GameBaseBean>();
    private GameMainAdapter gameMainAdapter;
    private GameDownLoadBusiness gameDownLoadBusiness;
    private boolean isPrepared = false;
    private boolean isVisiable = false;
    private boolean isFirst = true;
    private int pageNum = 1; //页数
    private boolean end = true;//是否最后一页
    private String version = null;//列表版本号
    private String gameChannelId;//游戏栏目ID
    private String gameListId;//游戏列表分页ID
    private boolean loadingData = false;//正在加载数据
    private boolean isLoadComplete=false;//数据加载完成


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity parentActivity = getActivity().getParent();
        Activity thisActivity = parentActivity != null ? parentActivity : getActivity();
        if (thisActivity.getParent() != null) {
            gameDownLoadBusiness = new GameDownLoadBusiness(thisActivity.getParent(), true, pagetype, null, null);
        } else {
            gameDownLoadBusiness = new GameDownLoadBusiness(thisActivity, true, pagetype, null, null);
        }
        gameMainAdapter = new GameMainAdapter(getContext(), dataList, gameDownLoadBusiness, pagetype,thisActivity);
        gridLayoutManager = new GridLayoutManager(getContext(), SPAN_COUNT, GridLayoutManager.VERTICAL, false);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {// 计算每个item的列宽
                return GameChannelUtil.getSpanSize(dataList, position, SPAN_COUNT);
            }
        });
    }

    @Override
    protected void initmView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.res_game_main_layout, container, false);
        isPrepared = true;
        ly_content = (LinearLayout) findViewById(R.id.ly_content);
        ly_error = (LinearLayout) findViewById(R.id.ly_error_view);
        pullToRefreshRecycle = (PullToRefreshRecycle) findViewById(R.id.game_result);
        pullToRefreshRecycle.setOnRefreshListener(this);
        recyclerView = pullToRefreshRecycle.getRefreshableView();
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(gameMainAdapter);
        ly_error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//网络错误
                if (loadingData) {
                    return;//正在加载数据，直接返回
                }
                loadingData = true;//true，正在加载数据
                version = null;
                pageNum = 1;
                loadData();
            }
        });
        if (isFirst) {//只加载一次数据
            isFirst = false;
            //readCache();//读取缓存
            loadData();//获取网络数据
        }
    }

    /**
     * 读取缓存
     */
//    private void readCache() {
//        String jsonStr = GameUtil.deserializeStr(getContext());
//        if (!TextUtils.isEmpty(jsonStr)) {
//            try {
//                JSONArray jsonArray = new JSONArray(jsonStr);
//                if (jsonArray.length() > 0) {
//                    fillDataList(jsonArray);
//                    gameMainAdapter.notifyItemRangeChanged(0, dataList.size());
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    /**
     * 加载数据
     */
    private void loadData() {
        ly_error.setVisibility(View.GONE);
        ly_content.setVisibility(View.VISIBLE);
        String url = NewVrUrlConfig.getGameUrlForChannel(gameChannelId, version);
        new AQuery(getContext()).ajax(url, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String json, AjaxStatus status) {
                loadingData = false;//false，加载数据完成
                pullToRefreshRecycle.onRefreshComplete();
                if (StringUtil.isEmpty(json)) {
                    if (dataList.size() == 0) {//没有数据
                        ly_error.setVisibility(View.VISIBLE);
                        ly_content.setVisibility(View.GONE);
                    }
                    return;
                }
                try {
                    JSONArray jsonArray = new JSONArray(json);
                    if (jsonArray.length() > 0) {
                        if (pageNum == 1) {//第一页
                            dataList.clear();
                        }
                        fillDataList(jsonArray);//填充dataList
                        isLoadComplete=true;
                        gameMainAdapter.notifyItemRangeChanged(0, dataList.size());

                        JSONObject jsonObject = jsonArray.optJSONObject(jsonArray.length() - 1);//最后一个
                        if (GameChannelUtil.GAME_LIST == jsonObject.getInt("type")) {//是资源列表
                            loadGameResourceList(jsonObject.getString("direct_resource"));//加载资源列表
                        }
                        GameUtil.serializeGameJSON(getContext(), json);// 更新本地缓存
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 填充dataList
     *
     * @param jsonArray
     */
    private void fillDataList(JSONArray jsonArray) {
        int position = 0;
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                int type = jsonObject.getInt("type");//视图类型
                if (GameChannelUtil.GAME_SPECIL_AD == type) {//专题广告位
                    if (!jsonObject.isNull("isshow") && (Integer.parseInt(jsonObject.getString("isshow")) == 1)) {
                        dataList.add(GameUtil.createGameTagBean(new GameTagBean(), jsonObject));
                    }
                    dataList.add(GameUtil.createGameAdBean(jsonObject));
                } else if (GameChannelUtil.GAME_WELFARE == type) {//福利三张图
                    if (!jsonObject.isNull("isshow") && (Integer.parseInt(jsonObject.getString("isshow")) == 1)) {
                        dataList.add(GameUtil.createGameTagBean(new GameTagBean(), jsonObject));
                    }
                    dataList.add(GameUtil.createGameWelfareBean(jsonObject));
                } else if (GameChannelUtil.GAME_THREE == type) {//3x1
                    if (!jsonObject.isNull("isshow") && (Integer.parseInt(jsonObject.getString("isshow")) == 1)) {
                        dataList.add(GameUtil.createGameTagBean(new GameTagBean(), jsonObject));
                    }
                    GameThreeBean gameThreeBean = GameUtil.createGameThreeBean(jsonObject);
                    loadGameThreeBean(gameThreeBean);//加载gameThreeBean
                } else if (GameChannelUtil.GAME_LIST == type) {//列表
                    //这个条件判断不能删掉
                } else if (GameChannelUtil.GAME_TWO == type) {//2*1
                    if (!jsonObject.isNull("isshow") && (Integer.parseInt(jsonObject.getString("isshow")) == 1)) {
                        dataList.add(GameUtil.createGameTagBean(new GameTagBean(), jsonObject));
                    }
                    dataList.add(GameUtil.createGameChannelBean(jsonObject, ++position));
                } else if (GameChannelUtil.GAME_SPECIL == type) {
                    if (!jsonObject.isNull("isshow") && (Integer.parseInt(jsonObject.getString("isshow")) == 1)) {
                        dataList.add(GameUtil.createGameTagBean(new GameTagBean(), jsonObject));
                    }
                    if (!jsonObject.isNull("specil_data") && jsonObject.optJSONArray("specil_data") != null && jsonObject.optJSONArray("specil_data").length() > 0) {
                        dataList.addAll(GameUtil.createGameResourceBeans(jsonObject));
                    }else{
                        dataList.add(GameUtil.createGameChannelBean(jsonObject, 0));
                    }
                } else if (GameChannelUtil.GAME_MULT_IMG == type) {
                    if (!jsonObject.isNull("isshow") && (Integer.parseInt(jsonObject.getString("isshow")) == 1)) {
                        dataList.add(GameUtil.createGameTagBean(new GameTagBean(), jsonObject));
                    }
                    if (!jsonObject.isNull("data") && jsonObject.optJSONArray("data") != null && jsonObject.optJSONArray("data").length() > 0) {
                        dataList.addAll(GameUtil.createGameChannelBeans(jsonObject, 0));
                    }
                } else if (GameChannelUtil.GAME_RECONNEND == type) {
                    loadGameRecommendBean(jsonObject);
                } else {//其他
                    dataList.add(GameUtil.createGameChannelBean(jsonObject, 0));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadGameRecommendBean(JSONObject jsonObject) {
        final ArrayList<GameRecommendBean> gameRecommendBeans = new ArrayList<GameRecommendBean>();
        for (int i = 0; i < 6; i++) {
            GameRecommendBean gameRecommendBean = new GameRecommendBean();
            gameRecommendBean.setType(9);
            gameRecommendBeans.add(gameRecommendBean);
        }
        try {
            if (!jsonObject.isNull("isshow") && (Integer.parseInt(jsonObject.getString("isshow")) == 1)) {
                dataList.add(GameUtil.createGameTagBean(new GameTagBean(), jsonObject));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dataList.addAll(gameRecommendBeans);
        final GameNextListBean gameNextListBean = new GameNextListBean();
        dataList.add(gameNextListBean);
        String url = "";
        if (!jsonObject.isNull("direct_resource")) {
            try {
                url = jsonObject.getString("direct_resource");
                if (TextUtils.isEmpty(url)) {
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        OkHttpUtil.getAsyn(url, new OkHttpUtil.ResultCallback<String>() {
            @Override
            public void onError(Request request, Exception e) {

            }

            @Override
            public void onResponse(String response, String url) {
                if (!TextUtils.isEmpty(response)) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (!jsonObject.isNull("data")) {
                            int size = jsonObject.getJSONArray("data").length();
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            gameNextListBean.setPageNum(1);
                            gameNextListBean.setEnd(false);
                            gameNextListBean.setType(99);
                            gameNextListBean.setVersion(jsonObject.getString("version"));
                            int len = size > 6 ? 6 : size;
                            int n = 0;
                            for (int i = 0; i < len; i++) {
                                GameUtil.createGameRecommendBean(gameRecommendBeans.get(i), jsonArray.optJSONObject(i), ++n);
                            }
                            gameMainAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 加载gameThreeBean
     *
     * @param gameThreeBean
     */
    private void loadGameThreeBean(final GameThreeBean gameThreeBean) {
        final GameResourceBean gameResourceBean = new GameResourceBean();
        final GameResourceBean gameResourceBean2 = new GameResourceBean();
        final GameResourceBean gameResourceBean3 = new GameResourceBean();
        gameResourceBean.setType(gameThreeBean.getType());
        gameResourceBean.setViewPosition(1);
        gameResourceBean2.setType(gameThreeBean.getType());
        gameResourceBean2.setViewPosition(2);
        gameResourceBean3.setType(gameThreeBean.getType());
        gameResourceBean3.setViewPosition(3);
        dataList.add(gameResourceBean);
        dataList.add(gameResourceBean2);
        dataList.add(gameResourceBean3);
        new AQuery(getContext()).ajax(gameThreeBean.getDirect_resource(), String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String json, AjaxStatus status) {
                if (!TextUtils.isEmpty(json)) {//不为空
                    try {
                        JSONArray jaData = new JSONObject(json).getJSONArray("data");
                        if (jaData != null && jaData.length() > 0) {
                            if (jaData.length() >= 1) {
                                GameUtil.createGameResourceBean(gameResourceBean, jaData.optJSONObject(0));
                            }
                            if (jaData.length() >= 2) {
                                GameUtil.createGameResourceBean(gameResourceBean2, jaData.optJSONObject(1));
                            }
                            if (jaData.length() >= 3) {
                                GameUtil.createGameResourceBean(gameResourceBean3, jaData.optJSONObject(2));
                            }
                            gameMainAdapter.notifyItemRangeChanged(0, dataList.size());
                            //SynchronizedDownload.updateDownloadForGame(getContext(), gameResourceBean,gameResourceBean2,gameResourceBean3);//更新本地数据库
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 加载资源列表
     */
    private void loadGameResourceList(String directResource) {
        new AQuery(getContext()).ajax(directResource, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String json, AjaxStatus status) {
                pullToRefreshRecycle.onRefreshComplete();
                if (!TextUtils.isEmpty(json)) {//不为空
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        gameListId = jsonObject.getString("id");
                        version = jsonObject.getString("version");
                        end = jsonObject.getBoolean("end");
                        JSONArray jaData = jsonObject.getJSONArray("data");
                        if (jaData != null && jaData.length() > 0) {
                            List<GameResourceBean> listData = new ArrayList<GameResourceBean>();
                            for (int i = 0; i < jaData.length(); i++) {
                                JSONObject joData = jaData.optJSONObject(i);
                                GameResourceBean gameBeanResource = GameUtil.createGameResourceBean(joData);
                                gameBeanResource.setType(GameChannelUtil.GAME_LIST);
                                listData.add(gameBeanResource);
                            }
                            int curSize = dataList.size();
                            dataList.addAll(listData);
                            gameMainAdapter.notifyItemRangeChanged(curSize, dataList.size());
                            SynchronizedDownload.updateDownloadForGame(getContext(), listData);//更新本地数据库
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onPullDownToRefresh(PullToRefreshBase<RecyclerView> pullToRefreshBase) {
        isLoadComplete=false;
        if (!NetworkUtil.isConnectInternet(getContext())) {
            pullToRefreshRecycle.onRefreshComplete();
            Toast.makeText(getContext(), "当前网络错误请检查网络设置", Toast.LENGTH_SHORT).show();
        } else if (loadingData) {//正在加载数据
            pullToRefreshRecycle.onRefreshComplete();
        } else {
            if (loadingData) {
                return;//正在加载数据，直接返回
            }
            loadingData = true;//true，正在加载数据
            version = null;
            pageNum = 1;
            loadData();
        }
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<RecyclerView> pullToRefreshBase) {
        if(!isLoadComplete){
            return;
        }
        if (!NetworkUtil.isConnectInternet(getContext())) {
            pullToRefreshRecycle.onRefreshComplete();
            Toast.makeText(getContext(), "当前网络错误请检查网络设置", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(version)) {
            pullToRefreshRecycle.onRefreshComplete();
        } else if (end) {
            pullToRefreshRecycle.onRefreshComplete();
            Toast.makeText(getContext(), "没有更多了!", Toast.LENGTH_SHORT).show();
        } else if (loadingData) {//正在加载数据
            pullToRefreshRecycle.onRefreshComplete();
        } else {
            pageNum++;
            loadGameResourceList(NewVrUrlConfig.getGameUrlForList(gameListId, version, pageNum));
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public GameMainActivity setTitle(String title) {
        setName(title);
        return this;
    }

    public String getPagetype() {
        return pagetype;
    }

    /**
     * 游戏栏目ID
     *
     * @param gameChannelId
     */
    public GameMainActivity setGameChannelId(String gameChannelId) {
        this.gameChannelId = gameChannelId;
        return this;
    }

    public GameMainActivity setPagetype(String pagetype) {
        this.pagetype = pagetype;
        return this;
    }

    @Override
    public void onResume() {
        if (isPrepared && isVisiable) {
            VrReportUtil.setDownLoadPageType(pagetype);
            VrReportUtil.reportPvTab();
        }
        super.onResume();
        if (isPrepared && isVisiable) {
            if (gameDownLoadBusiness != null) {
                gameDownLoadBusiness.unBindDownLoad();
            }
            gameDownLoadBusiness.initDownLoad();
            gameMainAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPause() {
        if (gameDownLoadBusiness != null) {
            gameDownLoadBusiness.unBindDownLoad();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisiable = true;
        } else {
            isVisiable = false;
        }
    }
}
