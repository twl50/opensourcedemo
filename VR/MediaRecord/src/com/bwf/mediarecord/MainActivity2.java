package com.bwf.mediarecord;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.VideoSource;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class MainActivity2 extends Activity implements OnClickListener {

	private SurfaceView preViewSv1;
	private SurfaceView preViewSv2;
	private Camera camera;
	// 点击控件的标签定义
	private final int REC_PIC = 0x111;
	private Button recLeft;
	private Button recRight;
	private boolean recording = false;
	private boolean isFirst = true;
	private MediaRecorder recorder;
	//camera工作状态
	private boolean camerawork = false;
	//屏幕尺寸参数
	private int screenWidth;
	private int screenHeight;
	private Rect gRect;
	//绘图消息号
		private final int DRAWBMP = 0x444;
		//绘图handler
		private Handler drawhandler = new Handler(){
			public void handleMessage(Message msg) {
				if (msg.what == DRAWBMP ) {
					//绘图线程
					Bitmap bmp = (Bitmap) msg.obj;
					FreshPreview(bmp);
				}
			}
		};
	/**
	 * handler处理从后台返回的Bitmap集合数据
	 */
	public Handler handler = new Handler() {

		public void handleMessage(Message msg) {

			HashMap<String, byte[]> map = (HashMap<String, byte[]>) msg.obj;
			// 从mSurfaceTexture中的onPreviewFrame中data经过handler一路传过来读data
            //判断当前录制状态
			if(msg.what == 5){	
            	String strTime = new String(map.get("result"));
            	strTime = String.copyValueOf(strTime.toCharArray(), 0, map.get("result").length);
            	//统一下时间格式标准
            	String time = format(strTime);
            	Log.e("recording", "true录制中。。。。。更新了时间");
            }else if(msg.what == 0){
            	Log.e("recording", "false已经停止了录制！");
            };		
			byte[] mData = map.get("data");
			Log.e("得到数据", "mData" + mData.length);
			if (camerawork != false) {
				Size size = camera.getParameters().getPreviewSize(); // 获取预览大小
				final int w = size.width/2; // 宽度
				final int h = size.height;
				final YuvImage image = new YuvImage(mData, ImageFormat.NV21, w, h, null);
				ByteArrayOutputStream os = new ByteArrayOutputStream(mData.length);
				if (!image.compressToJpeg(new Rect(0, 0, w, h), 100, os)) {
					return;
				}
				byte[] tmp = os.toByteArray();
				// byte[] imgByte = Base64.decode(mData, Base64.DEFAULT);
				// 一直空
				Bitmap bmp = BitmapFactory.decodeByteArray(tmp, 0, tmp.length);
				
				Message bmpmsg = new Message();
				bmpmsg.what = DRAWBMP;
				bmpmsg.obj = bmp;
			    drawhandler.sendMessage(bmpmsg);
			}

		}
		
       /**
        * 对时间进行格式化
        * @param strTime
        * @return
        */
		private String format(String strTime) {
			String time = null;
            if (strTime.length()<=2) {
				time = "00:00:"+strTime;
			}else if(strTime.length()<=5){
				time = "00:"+strTime;
			}else{
				time = strTime;
			}
			return time;
		};
	};
	private MySurfaceTexture mySurfaceTexture;
	
	
	/**
	 * 显示哪个surfaceview,双缓存
	 */
	private void FreshPreview(Bitmap bmp) {
		Log.e("IsfrontPre", "开始一次绘图");
		if (bmp != null)
		
		synchronized (preViewSv1.getHolder()) {
			Canvas canvas = preViewSv1.getHolder().lockCanvas();

			if (canvas != null) {
				canvas.drawBitmap(bmp, null, gRect, null);
				preViewSv1.getHolder().unlockCanvasAndPost(canvas);
			}
		}
		synchronized (preViewSv2.getHolder()) {
			Canvas canvas = preViewSv2.getHolder().lockCanvas();

			if (canvas != null) {
				canvas.drawBitmap(bmp, null, gRect, null);
				preViewSv2.getHolder().unlockCanvasAndPost(canvas);
			} 
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		ViewGroup baseView = ((ViewGroup) View
				.inflate(this, R.layout.activity_record, null));
		
		LayoutParams params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;

		// 获取两个layout，实际上长的是一样的
		View leftPart = View.inflate(this, R.layout.record_part, null);
		View rightPart = View.inflate(this, R.layout.record_part, null);
		// 将两个view 加到baseview上
		baseView.addView(leftPart, params);
		baseView.addView(rightPart, params);

		setContentView(baseView);
		
		
		//屏幕尺寸
		DisplayMetrics dm = getResources().getDisplayMetrics();
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
		gRect = new Rect(0, 0, screenWidth, screenHeight);
		preViewSv1 = (SurfaceView) leftPart.findViewById(R.id.preView_sv1);
		recLeft = (Button) leftPart.findViewById(R.id.record_bt);
		preViewSv2 = (SurfaceView) rightPart.findViewById(R.id.preView_sv1);
		recRight = (Button) rightPart.findViewById(R.id.record_bt);
		preViewSv1.getHolder().addCallback(new MyCallBacke());
		preViewSv1.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		preViewSv2.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		preViewSv1.getHolder().setFormat(PixelFormat.TRANSLUCENT);
		preViewSv1.setKeepScreenOn(true);
		camera = Camera.open();
		recorder = new MediaRecorder();
		recorder.setCamera(camera);

		//监听器事件
		recLeft.setTag(REC_PIC);
		recLeft.setOnClickListener(this);
		recRight.setTag(REC_PIC);
		recRight.setOnClickListener(this);
		
		mySurfaceTexture = new MySurfaceTexture(10, handler);
	}
	
	private class MyCallBacke implements SurfaceHolder.Callback{

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			try {
//				camera.setPreviewDisplay(preViewSv.getHolder());
//				camera.startPreview();
				if (!isFirst) {
					camera = Camera.open();
					mySurfaceTexture.setRecord(false);
				}
				camerawork = true;
				mySurfaceTexture.startpre(camera);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

			
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			camerawork = false;
			camera.setPreviewCallback(null);
			camera.stopPreview();//停止预览
			camera.release();
			camera = null;
		}
		
	}

	@Override
	public void onClick(View v) {
		if ((Integer) v.getTag()==REC_PIC) {//
			if (!recording) {
				if (!isFirst) {
					recorder = new MediaRecorder();
					if (camera!=null) {
						camera.setPreviewCallback(null);
						try {
							camera.setPreviewTexture(null);
						} catch (IOException e) {
							e.printStackTrace();
						}
						 camera.stopPreview();
                         camera.release();
                         camera = null;
                         mySurfaceTexture.release();
                         mySurfaceTexture = null;
                         camera= camera.open();
                         recorder.setCamera(camera);
                         mySurfaceTexture = new MySurfaceTexture(10, handler);
                         mySurfaceTexture.setRecord(true);
                         mySurfaceTexture.startpre(camera);
                         camera.unlock();	//第一步 解锁摄像头, 否则不能录像
					}
				}else{
					isFirst = false;
					camera.unlock();	//第一步 解锁摄像头, 否则不能录像
				}
			
				//设置音频源输入
				 recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				 //设置视屏源输入
				 recorder.setVideoSource(VideoSource.CAMERA);	
				// 设置录像质量	
				 recorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_720P));
//				 recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//				 recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//				 recorder.setPreviewDisplay(preViewSv1.getHolder().getSurface());	
				 
				 recorder.setOutputFile("/mnt/sdcard/" + "888bwf"+System.currentTimeMillis() + ".mp4");
				 try {
					recorder.prepare();
				} catch (Exception e) {
					e.printStackTrace();
				} 
				 recorder.start();   // Recording is now started
				recLeft.setText("stop_record");
				recRight.setText("stop_record");
				recording = true;
			}else{
				recorder.stop();
				recorder.reset();   
				recorder.release(); 
				camera.lock();
				mySurfaceTexture.setRecord(false);
				recLeft.setText("start_record");
				recRight.setText("start_record");
				recording = false;
			}
		}
	}
}
