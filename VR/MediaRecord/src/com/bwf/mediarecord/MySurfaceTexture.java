package com.bwf.mediarecord;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.VideoSource;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceView;


/**
 * 该类没有界面，可以在创建后后台获取照相机的数据，
 * 这里通过Handler发送回调用者
 * 提供方法：开始相机，拍照，录像，停止相机等
 * @author LDD
 *
 */
public class MySurfaceTexture extends SurfaceTexture implements  Camera.PreviewCallback{

	private Handler handler;
	private boolean recording = false;  //是否正在录像
	private Thread timeThread;

	public MySurfaceTexture(int texName,Handler handler) {
		super(texName);
		this.handler = handler;
	}
	/**
	 * 开始预览，初始化相机
	 */
	public void startpre(Camera camera){
		try {
			camera.setPreviewCallback(this);     
			camera.setPreviewTexture(this);
			camera.startPreview();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
    
    /**
     * mSurfaceTexture中继承接口于是有该回调方法
     * 回调方法取到数据源data
     */
	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		//拿到帧数据，给doSomethingNeeded处理
		MySurfaceTexture.this.doSomethingNeeded(data);
		Log.e("doSomethingNeeded打开", "data数组已经发给它");
	}
	public void setRecord(boolean recording){
		this.recording = recording;
	}
	   /**
	    * 对获取到的位图进行处理方法,简单的发出去
	    * @param bmp  实时的视屏位图
	    */
	public void doSomethingNeeded(byte[] data) {

		Message msg = new Message();
		Map<String, byte[]> map = new HashMap<String, byte[]>();
		map.put("data", data);
		//让获得消息的人能够判断是不是在录像，以便更新UI
		//拍照请忽视
		if(recording == false){
			msg.what = 0;
		}else{
			msg.what = 5;
			//把时间也发过去
			map.put("result", result.getBytes());
			Log.e("result =", result);
		}
		msg.obj = map;
		handler.sendMessage(msg);
		Log.e("handler", "data数组通过handler发给Activity了");
	}

	
	/**
	 *  定时器设置，实现计时 
	 **/
	private int second = 0;
	private int s; //秒位
	private int m;  //分位
	private int h;  //时位
	private String result ="00:00:00";   //要显示的格式字符串
	private Handler Timehandler = new Handler();
	private boolean bool = true ;
	private Runnable task = new Runnable() {
		public void run() {
			if (bool) {
				handler.postDelayed(this, 1000);
				second ++;
				if (second < 60) {
					result = format(second);
				} else if (second < 3600) {
					m = second / 60;
					s = second % 60;
					result = format(m)+":"+format(s);
				} else {
					h = second / 3600;
					m = (second % 3600) / 60;
					s = (second % 3600) % 60;
					result =format(h)+":"+format(m)+":"+format(s);
				}
			}
		}
	};

	/** 格式化时间 */

	public String format(int i) {
		String s = i + "";
		if (s.length() == 1) {
			s = "0" + s;
		}
		return s;
	}

 

}
