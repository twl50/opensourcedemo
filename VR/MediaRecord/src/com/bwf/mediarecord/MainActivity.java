package com.bwf.mediarecord;

import java.io.IOException;

import android.app.Activity;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.VideoSource;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	private SurfaceView preViewSv;
	private Camera camera;
	private Button recordBt;

	private boolean recording = false;
	private boolean isFirst = true;
	private MediaRecorder recorder;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		preViewSv = (SurfaceView) findViewById(R.id.preView_sv);
		preViewSv.getHolder().addCallback(new MyCallBacke());
		preViewSv.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		camera = Camera.open();
		recorder = new MediaRecorder();
		recorder.setCamera(camera);
		recordBt = (Button) findViewById(R.id.record_bt);
		recordBt.setOnClickListener(this);
	}
	
	private class MyCallBacke implements SurfaceHolder.Callback{

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			try {
				camera.setPreviewDisplay(preViewSv.getHolder());
				camera.startPreview();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

			
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			camera.stopPreview();//停止预览
			camera.release();
			camera = null;
		}
		
	}

	@Override
	public void onClick(View v) {
		if (v.getId()==R.id.record_bt) {//
			if (!recording) {
				if (!isFirst) {
					recorder = new MediaRecorder();
					if (camera!=null) {
						 camera.stopPreview();
                         camera.release();
                         camera = null;
                         camera= camera.open();
                         recorder.setCamera(camera);
                         camera.unlock();	//第一步 解锁摄像头, 否则不能录像
					}
				}else{
					isFirst = false;
					camera.unlock();	//第一步 解锁摄像头, 否则不能录像
				}
			
				//设置音频源输入
				 recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				 //设置视屏源输入
				 recorder.setVideoSource(VideoSource.CAMERA);	
				// 设置录像质量	
				 recorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_720P));
//				 recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//				 recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//				 recorder.setPreviewDisplay(preViewSv.getHolder().getSurface());	
				 
				 recorder.setOutputFile("/mnt/sdcard/" + "888bwf"+System.currentTimeMillis() + ".mp4");
				 try {
					recorder.prepare();
				} catch (Exception e) {
					e.printStackTrace();
				} 
				 recorder.start();   // Recording is now started
				recordBt.setText("stop_record");
				recording = true;
			}else{
				recorder.stop();
				recorder.reset();   
				recorder.release(); 
				camera.lock();
				recordBt.setText("start_record");
				recording = false;
			}
		}
	}
}
