package com.example.wifidemo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnScrollChangeListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity implements OnClickListener {

	private static final int REFRESH_CONN = 100;
	protected boolean isUpdate = true;

	public int showSelectPosition = 0;
	// Wifi管理类
	private WifiAdmin mWifiAdmin;
	// 扫描结果列表
	private List<ScanResult> list = new ArrayList<ScanResult>();

	private ViewGroup baseView;
	private LinearLayout leftPart;
	private LinearLayout rightPart;
	private LinearLayout.LayoutParams params;

	private ListView l_listView;
	private TextView l_tvState;
	private ToggleButton l_tgbWifiSwitch;

	private ListView r_listView;
	private TextView r_tvState;
	private ToggleButton r_tgbWifiSwitch;

	private MyListViewAdapter mAdapter;

	// ---------------- 测试按钮 begin -----------------------
	private int position;

	private static final int BTN_LEFT = 101;
	private static final int BTN_RIGHT = 102;
	private static final int BTN_OK = 103;
	private static final int BTN_BACK = 104;

	private Button l_btnLeft;
	private Button l_btnRight;
	private Button l_btnOk;
	private Button l_btnBack;
	private Button r_btnLeft;
	private Button r_btnRight;
	private Button r_btnOk;
	private Button r_btnBack;
	// ---------------- 测试按钮 end -----------------------

	private Handler mHandler = new Handler() {

		public void handleMessage(Message msg) {

			switch (msg.what) {
			case REFRESH_CONN:
				MainActivity.this.getWifiListInfo();
				MainActivity.this.mAdapter.setDatas(MainActivity.this.list);
				MainActivity.this.mAdapter.notifyDataSetChanged();
				break;

			// ---------------- 测试按钮 begin -----------------------
			case BTN_LEFT:
			case BTN_RIGHT:
				mAdapter.setShowSelect(position);
				l_listView.setSelection(position);
				r_listView.setSelection(position);
				break;
			case BTN_OK:

				break;
			case BTN_BACK:

				break;
			// ---------------- 测试按钮 end -----------------------
			}
			super.handleMessage(msg);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 获取装左右两边内容的容器
		baseView = ((ViewGroup) View.inflate(this, R.layout.activity_base, null));
		params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;

		leftPart = ((LinearLayout) View.inflate(this, R.layout.activity_main, null));
		rightPart = ((LinearLayout) View.inflate(this, R.layout.activity_main, null));

		baseView.addView(leftPart, params);
		baseView.addView(rightPart, params);

		setContentView(baseView);

		initData();
		initView();
		setListener();

		refreshWifiStatusOnTime();
	}

	// 初始化数据
	private void initData() {
		mWifiAdmin = new WifiAdmin(MainActivity.this);
		// 获得Wifi列表信息
		getWifiListInfo();
	}

	// 初始化视图
	private void initView() {

		l_tgbWifiSwitch = (ToggleButton) leftPart.findViewById(R.id.tgb_wifi_switch);
		l_listView = (ListView) leftPart.findViewById(R.id.lv_wifi_msg);
		l_tvState = (TextView) leftPart.findViewById(R.id.tv_state);

		r_tgbWifiSwitch = (ToggleButton) rightPart.findViewById(R.id.tgb_wifi_switch);
		r_listView = (ListView) rightPart.findViewById(R.id.lv_wifi_msg);
		r_tvState = (TextView) rightPart.findViewById(R.id.tv_state);

		mAdapter = new MyListViewAdapter(this, list);
		l_listView.setAdapter(mAdapter);
		r_listView.setAdapter(mAdapter);

		// ---------------- 测试按钮 begin -----------------------

		l_btnLeft = (Button) leftPart.findViewById(R.id.btn_left);
		l_btnRight = (Button) leftPart.findViewById(R.id.btn_right);
		l_btnOk = (Button) leftPart.findViewById(R.id.btn_ok);
		l_btnBack = (Button) leftPart.findViewById(R.id.btn_back);
		r_btnLeft = (Button) rightPart.findViewById(R.id.btn_left);
		r_btnRight = (Button) rightPart.findViewById(R.id.btn_right);
		r_btnOk = (Button) rightPart.findViewById(R.id.btn_ok);
		r_btnBack = (Button) rightPart.findViewById(R.id.btn_back);

		// ---------------- 测试按钮 end -----------------------

		int wifiState = mWifiAdmin.checkState();
		if (wifiState == WifiManager.WIFI_STATE_DISABLED || wifiState == WifiManager.WIFI_STATE_DISABLING
				|| wifiState == WifiManager.WIFI_STATE_UNKNOWN) {
			l_tgbWifiSwitch.setChecked(false);
			r_tgbWifiSwitch.setChecked(false);
			l_tvState.setText("关");
			r_tvState.setText("关");
		} else {
			l_tgbWifiSwitch.setChecked(true);
			r_tgbWifiSwitch.setChecked(true);
			l_tvState.setText("开");
			r_tvState.setText("开");
		}
	}

	// 设置监听
	private void setListener() {

		// ---------------- 测试按钮 begin -----------------------

		l_btnLeft.setOnClickListener(this);
		l_btnRight.setOnClickListener(this);
		l_btnOk.setOnClickListener(this);
		l_btnBack.setOnClickListener(this);
		r_btnLeft.setOnClickListener(this);
		r_btnRight.setOnClickListener(this);
		r_btnOk.setOnClickListener(this);
		r_btnBack.setOnClickListener(this);

		// ---------------- 测试按钮 end -----------------------

		OnCheckedChangeListener tgbWifiSwitchListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					mWifiAdmin.openWifi();
					l_tgbWifiSwitch.setChecked(true);
					r_tgbWifiSwitch.setChecked(true);
				} else {
					mWifiAdmin.closeWifi();
					l_tgbWifiSwitch.setChecked(false);
					r_tgbWifiSwitch.setChecked(false);
				}
			}
		};

		l_tgbWifiSwitch.setOnCheckedChangeListener(tgbWifiSwitchListener);
		r_tgbWifiSwitch.setOnCheckedChangeListener(tgbWifiSwitchListener);

		OnItemClickListener listViewListener = new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				ScanResult scanResult = list.get(position);

				String desc = "";
				String descOri = scanResult.capabilities;
				if (descOri.toUpperCase().contains("WPA-PSK")) {
					desc = "WPA";
				}
				if (descOri.toUpperCase().contains("WPA2-PSK")) {
					desc = "WPA2";
				}
				if (descOri.toUpperCase().contains("WPA-PSK") && descOri.toUpperCase().contains("WPA2-PSK")) {
					desc = "WPA/WPA2";
				}

				if (desc.equals("")) {
					isConnectSelf(scanResult);
					return;
				}
				isConnect(scanResult);
			}

			private void isConnect(ScanResult scanResult) {
				if (mWifiAdmin.isConnect(scanResult)) {
					// 已连接，弹出显示连接状态页面
					Intent intent = new Intent(MainActivity.this, ConnWifiActivity.class);
					startActivityForResult(intent, 0);
				} else {
					// 未连接，弹出显示连接输入对话框
					Intent intent = new Intent(MainActivity.this, UnConnWifiActivity.class);
					intent.putExtra("ScanResult", scanResult);
					startActivityForResult(intent, 0);
				}
			}

			private void isConnectSelf(ScanResult scanResult) {
				if (mWifiAdmin.isConnect(scanResult)) {
					// 已连接，弹出显示连接状态页面
					Intent intent = new Intent(MainActivity.this, ConnWifiActivity.class);
					startActivityForResult(intent, 0);
				} else {
					boolean iswifi = mWifiAdmin.connectSpecificAP(scanResult);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (iswifi) {
						Toast.makeText(MainActivity.this, "连接成功！", 0).show();
					} else {
						Toast.makeText(MainActivity.this, "连接失败！", 0).show();
					}
				}
			}
		};

		l_listView.setOnItemClickListener(listViewListener);
		r_listView.setOnItemClickListener(listViewListener);
		
	}

	// 定时刷新Wifi信息
	private void refreshWifiStatusOnTime() {
		new Thread() {
			public void run() {
				while (isUpdate) {
					try {
						// 默认设置每10秒刷新一次
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					mHandler.sendEmptyMessage(REFRESH_CONN);
				}
			}
		}.start();
	}

	// 获取wifi信息
	private void getWifiListInfo() {
		mWifiAdmin.startScan();
		List<ScanResult> tmpList = mWifiAdmin.getWifiList();
		if (tmpList == null) {
			list.clear();
		} else {
			list = tmpList;
		}
	}

	// 跳转操作结束后，返回刷新页面
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		getWifiListInfo();
		mAdapter.setDatas(list);
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// ---------------- 测试按钮 begin -----------------------
		case R.id.btn_left:
			position=position<=0?list.size()-1:--position;
			mHandler.sendEmptyMessage(BTN_LEFT);
			break;
		case R.id.btn_right:
			position=position>=list.size()-1?0:++position;
			mHandler.sendEmptyMessage(BTN_RIGHT);
			break;
		case R.id.btn_ok:

			break;
		case R.id.btn_back:

			break;
		// ---------------- 测试按钮 end -----------------------
		}
	}
}
