package com.example.wifidemo;

import java.util.List;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyListViewAdapter extends BaseAdapter {

	private List<ScanResult> datas;
	private Context context;
	private int selectPosition;
	// 取得WifiManager对象
	private WifiManager mWifiManager;
	ConnectivityManager cm;

	public void setDatas(List<ScanResult> datas) {
		this.datas = datas;
	}

	public MyListViewAdapter(Context context, List<ScanResult> datas) {
		super();
		this.datas = datas;
		this.context = context;
		mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	}

	public void setShowSelect(int selectPosition){
		this.selectPosition = selectPosition;
		notifyDataSetChanged();
	}
	@Override
	public int getCount() {
		if (datas == null) {
			return 0;
		}
		return datas.size();
	}

	@Override
	public Object getItem(int position) {
		return datas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		Log.e("MainActivity", "getView()---"+position);
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.wifi_info_item, null);
			holder = new ViewHolder();
			holder.tvWifiName = (TextView) convertView.findViewById(R.id.tv_wifi_name);
			holder.tvWifiState = (TextView) convertView.findViewById(R.id.tv_wifi_state);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// 设置WIFI名字
		holder.tvWifiName.setText(datas.get(position).SSID);
		if (selectPosition == position) {
			Log.e("MainActivity", "设置对应的tiem 为选中"+position);
			holder.tvWifiName.setEnabled(true);
		}else{
			holder.tvWifiName.setEnabled(false);
		}
		

		// 设置连接WIFI
		State wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
		if (wifi == State.CONNECTED) {
			WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
			String g1 = wifiInfo.getSSID();

			String g2 = "\"" + datas.get(position).SSID + "\"";

			if (g2.endsWith(g1)) {
				holder.tvWifiState.setText("已连接");
			} else {
				holder.tvWifiState.setText("");
			}
		} else {
			holder.tvWifiState.setText("");
		}
		return convertView;
	}

	public static class ViewHolder {
		public TextView tvWifiName;
		public TextView tvWifiState;
	}
	
}