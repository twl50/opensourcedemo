package com.example.wifidemo;

import com.example.wifidemo.WifiAdmin.WifiCipherType;

import android.app.Activity;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UnConnWifiActivity extends Activity implements OnClickListener {

	private ScanResult scanResult;

	private ViewGroup baseView;
	private RelativeLayout leftPart;
	private RelativeLayout rightPart;
	private LinearLayout.LayoutParams params;

	private TextView l_tvWifiLevel;
	private EditText l_etWifiPass;
	private Button l_btnCancel;
	private Button l_btnConnect;
	
	private TextView r_tvWifiLevel;
	private EditText r_etWifiPass;
	private Button r_btnCancel;
	private Button r_btnConnect;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// 获取装左右两边内容的容器
		baseView = ((ViewGroup) View.inflate(this, R.layout.activity_base, null));
		params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;

		leftPart = ((RelativeLayout) View.inflate(this, R.layout.activity_nuconn_wifi, null));
		rightPart = ((RelativeLayout) View.inflate(this, R.layout.activity_nuconn_wifi, null));

		baseView.addView(leftPart, params);
		baseView.addView(rightPart, params);

		setContentView(baseView);

		initView();
		initData();
		setListener();
	}

	private void initView() {
		l_tvWifiLevel = (TextView) leftPart.findViewById(R.id.tv_wifi_level);
		l_etWifiPass = (EditText) leftPart.findViewById(R.id.et_wifi_pass);
		l_btnCancel = (Button) leftPart.findViewById(R.id.btn_cancel);
		l_btnConnect = (Button) leftPart.findViewById(R.id.btn_connect);
		
		r_tvWifiLevel = (TextView) rightPart.findViewById(R.id.tv_wifi_level);
		r_etWifiPass = (EditText) rightPart.findViewById(R.id.et_wifi_pass);
		r_btnCancel = (Button) rightPart.findViewById(R.id.btn_cancel);
		r_btnConnect = (Button) rightPart.findViewById(R.id.btn_connect);
	}

	private void initData() {
		Intent intent = getIntent();
		scanResult = intent.getParcelableExtra("ScanResult");

		l_tvWifiLevel.setText(WifiAdmin.singlLevToStr(scanResult.level));
		r_tvWifiLevel.setText(WifiAdmin.singlLevToStr(scanResult.level));
	}

	private void setListener() {
		l_btnCancel.setOnClickListener(this);
		l_btnConnect.setOnClickListener(this);
		r_btnCancel.setOnClickListener(this);
		r_btnConnect.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_cancel:
			finish();
			break;
		case R.id.btn_connect:
			WifiCipherType type = null;
			if (scanResult.capabilities.toUpperCase().contains("WPA")) {
				type = WifiCipherType.WIFICIPHER_WPA;
			} else if (scanResult.capabilities.toUpperCase().contains("WEP")) {
				type = WifiCipherType.WIFICIPHER_WEP;
			} else {
				type = WifiCipherType.WIFICIPHER_NOPASS;
			}

			// 连接网络
			WifiAdmin mWifiAdmin = new WifiAdmin(this);
			boolean bRet = mWifiAdmin.connect(scanResult.SSID, l_etWifiPass.getText().toString().trim(), type);
			if (bRet) {
				Toast.makeText(getApplicationContext(), "连接成功", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(), "连接失败", Toast.LENGTH_SHORT).show();
			}
			finish();
			break;
		}
	}
}
