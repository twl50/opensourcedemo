package com.example.wifidemo;

import android.app.Activity;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ConnWifiActivity extends Activity implements OnClickListener {

	private WifiAdmin mWifiAdmin;
	private WifiManager my_wifiManager;
	private DhcpInfo dhcpInfo;

	private ViewGroup baseView;
	private RelativeLayout leftPart;
	private RelativeLayout rightPart;
	private LinearLayout.LayoutParams params;

	private TextView l_tvIpaddress;
	private TextView l_tvIpmask;
	private Button l_btnForget;
	private Button l_btnCancel;

	private TextView r_tvIpaddress;
	private TextView r_tvIpmask;
	private Button r_btnForget;
	private Button r_btnCancel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conn_wifi);

		// 获取装左右两边内容的容器
		baseView = ((ViewGroup) View.inflate(this, R.layout.activity_base, null));
		params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;

		leftPart = ((RelativeLayout) View.inflate(this, R.layout.activity_conn_wifi, null));
		rightPart = ((RelativeLayout) View.inflate(this, R.layout.activity_conn_wifi, null));

		baseView.addView(leftPart, params);
		baseView.addView(rightPart, params);

		setContentView(baseView);

		initData();
		initView();
		setListener();
	}

	private void initData() {
		my_wifiManager = ((WifiManager) getSystemService("wifi"));
		dhcpInfo = my_wifiManager.getDhcpInfo();
		this.mWifiAdmin = new WifiAdmin(this);
	}

	private void initView() {
		l_tvIpaddress = (TextView) leftPart.findViewById(R.id.tv_ipaddress);
		l_tvIpmask = (TextView) leftPart.findViewById(R.id.tv_ipmask);
		l_btnForget = (Button) leftPart.findViewById(R.id.btn_forget);
		l_btnCancel = (Button) leftPart.findViewById(R.id.btn_cancel);

		r_tvIpaddress = (TextView) rightPart.findViewById(R.id.tv_ipaddress);
		r_tvIpmask = (TextView) rightPart.findViewById(R.id.tv_ipmask);
		r_btnForget = (Button) rightPart.findViewById(R.id.btn_forget);
		r_btnCancel = (Button) rightPart.findViewById(R.id.btn_cancel);

		l_tvIpaddress.setText(intToIp(dhcpInfo.ipAddress));
		l_tvIpmask.setText(intToIp(dhcpInfo.netmask));
		r_tvIpaddress.setText(intToIp(dhcpInfo.ipAddress));
		r_tvIpmask.setText(intToIp(dhcpInfo.netmask));
	}

	private void setListener() {
		l_btnForget.setOnClickListener(this);
		l_btnCancel.setOnClickListener(this);
		r_btnForget.setOnClickListener(this);
		r_btnCancel.setOnClickListener(this);
	}

	private String intToIp(int paramInt) {
		return (paramInt & 0xFF) + "." + (0xFF & paramInt >> 8) + "." + (0xFF & paramInt >> 16) + "."
				+ (0xFF & paramInt >> 24);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_forget:
			int netId = mWifiAdmin.getConnNetId();
			mWifiAdmin.disConnectionWifi(netId);
			finish();
			break;
		case R.id.btn_cancel:
			finish();
			break;
		}
	}
}
