package com.booston.lancher3d.camera;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;


import com.booston.lancher3d.activity.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.SurfaceHolder.Callback;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * 在该类中加载相机相关的组件，并提供访问器，供Activity使用
 * @author LDD
 *
 */
public class Camera_View extends LinearLayout {

	private View view;
	private Context context;
	private ImageView picturePreview;
	private SurfaceView previewSV1;
	private Button takepicture;

	//private final int RESULT_OK = 0x333;
    private Bitmap gBitmap;
	private Rect gRect;
	//private int screenHeight;
	//private int screenWidth;

	
	/**
	 * SurfaceView/ImageView/Button等3个组件读访问器
	 * @return
	 */
	public ImageView getPicturePreview() {
		return picturePreview;
	}
	public void setPicturePreview(ImageView picturePreview) {
		this.picturePreview = picturePreview;
	}
	public SurfaceView getPreviewSV1() {
		return previewSV1;
	}
	public void setPreviewSV1(SurfaceView previewSV) {
		this.previewSV1 = previewSV1;
	}
	public Button getTakepicture() {
		return takepicture;
	}
	public void setTakepicture(Button takepicture) {
		this.takepicture = takepicture;
	}


	/**
	 * 构造方法构造View
	 * @param context
	 */
	public Camera_View(Context context,int Rid,Rect gRect) {
		super(context);
		this.context = context;
		this.gRect = gRect;
		//设置view
		setContentView(Rid);
		//加载view
		Log.e("调用了几次", "调用加一次");
		initView();
	}
	/**
	 * 设置布局器View
	 * @param Rid
	 */
    public void setContentView(int Rid){
        view = null;
            view = LayoutInflater.from(getContext()).inflate(Rid, null);
        removeAllViews();
        addView(view);
    }
	
	
	/**
	 * 加载surfaceView
	 */
	private void initView() {
		if(view == null)return;      //以防空指针
		
		//**************************/surfaceView\*******************************
		previewSV1 = (SurfaceView) view.findViewById(R.id.surfaceView1);
		previewSV1.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		previewSV1.setVisibility(SurfaceView.VISIBLE);
		previewSV1.getHolder().setKeepScreenOn(true);// 屏幕常亮 

		//*****************************/button和timeText\***********************
		//拍照布局
		RelativeLayout buttonLayout = (RelativeLayout) view.findViewById(R.id.buttonLayout);
		//拍照按钮
		takepicture=(Button)buttonLayout.findViewById(R.id.takepicture);

		//照片预览
		 picturePreview= (ImageView)buttonLayout.findViewById(R.id.scalePic);
		
	}

//
//	/**
//	 * 保存好了图片需要发个广播，否则图库不能及时刷新,发现没什么用
//	 * 并且把拍到的图片作为信息返回给请求者
//	 * @param Ldd
//	 */
//	public void fileScan(String fName){ 
//		imageUri = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)+fName); 	
//		Intent intentBc = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//		intentBc.setData(imageUri);    
//		Log.e("返回2", imageUri+"");
//		((Activity) context).sendBroadcast(intentBc); 
//		
//	
//	} 
//	
//	/**
//	 *  提供一个静态方法，用于根据手机方向获得相机预览画面旋转的角度  
//	 * @param activity
//	 * @return
//	 */
//    public static int getPreviewDegree(Activity activity) {  
//        // 获得手机的方向  
//        int rotation = activity.getWindowManager().getDefaultDisplay()  
//                .getRotation();  
//        int degree = 0;  
//        // 根据手机的方向计算相机预览画面应该选择的角度  
//        switch (rotation) {  
//        case Surface.ROTATION_0:  
//            degree = 90;  
//            break;  
//        case Surface.ROTATION_90:  
//            degree = 0;  
//            break;  
//        case Surface.ROTATION_180:  
//            degree = 270;  
//            break;  
//        case Surface.ROTATION_270:  
//            degree = 180;  
//            break;  
//        }  
//        return degree;  
//    }


}
