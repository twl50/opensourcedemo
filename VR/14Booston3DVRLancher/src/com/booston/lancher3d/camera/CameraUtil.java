package com.booston.lancher3d.camera;

import java.io.IOException;

import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.util.Log;

/**
 * 自定义的对系统camera的操作类
 * @author PC
 *
 */
public class CameraUtil {

	public static Camera camera;
	public static Size cameraSize;
	public static boolean openCamera(){
		camera = Camera.open();
		if (camera == null) {
			return false;
		}else{
			return true;
		}
	}
	public static boolean releaseCamera(){
		camera.release();
		camera = null;
		if (camera == null) {
			return true;
		}else{
			return false;
		}
	}
	public static boolean unlock(){
		camera.unlock();
		return true;
		
	}
	public static boolean lock(){
		camera.lock();
		return true;
	}
}
