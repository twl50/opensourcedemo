package com.booston.lancher3d.camera;


import java.io.IOException;

import com.booston.lancher3d.activity.R;

import android.content.Context;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.VideoSource;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Record_View extends LinearLayout {

	private Context context;
	private View view;

	private TextView time_txt;
	private Button recordBt;
	private SurfaceView preViewSv1;


	/*
	 * 4个组件的访问器
	 */
	public TextView getTime_txt() {
		return time_txt;
	}
	public void setTime_txt(TextView time_txt) {
		this.time_txt = time_txt;
	}
	public Button getRecordBt() {
		return recordBt;
	}
	public void setRecordBt(Button recordBt) {
		this.recordBt = recordBt;
	}
	public SurfaceView getPreViewSv1() {
		return preViewSv1;
	}
	public void setPreViewSv1(SurfaceView preViewSv) {
		this.preViewSv1 = preViewSv;
	}

	/**
	 * 构造方法，通过id构造子view
	 * @param context
	 * @param Rid
	 */
	public Record_View(Context context,int Rid){
		super(context);
		this.context = context;
		//设置view
		setContentView(Rid);
		//加载view
		initView();
	}
    /**
     * 根据ID匹配布局的view
     * @param Rid
     */
	private void setContentView(int Rid) {
        view = null;
        view = LayoutInflater.from(getContext()).inflate(Rid, null);
        removeAllViews();
        addView(view);
	}
	/**
	 * 加载控件
	 */
	private void initView() {

		preViewSv1 = (SurfaceView)view.findViewById(R.id.preView_sv1);
		preViewSv1.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


		recordBt = (Button)view.findViewById(R.id.record_bt);
		time_txt = (TextView)view.findViewById(R.id.time_txt);

	}

}
