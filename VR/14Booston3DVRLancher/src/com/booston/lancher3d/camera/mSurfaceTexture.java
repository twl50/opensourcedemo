package com.booston.lancher3d.camera;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.VideoSource;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceView;


/**
 * 该类没有界面，可以在创建后后台获取照相机的数据，
 * 这里通过Handler发送回调用者
 * 提供方法：开始相机，拍照，录像，停止相机等
 * @author LDD
 *
 */
public class mSurfaceTexture extends SurfaceTexture implements Camera.PictureCallback, Camera.PreviewCallback{

	private Camera gCamera;
	private Handler handler;
	private MediaRecorder recorder;
	private boolean recording = false;  //是否正在录像
	private Thread timeThread;

	public mSurfaceTexture(int texName,Handler handler) {
		super(texName);
		this.handler = handler;
	}
	/**
	 * 开始预览，初始化相机
	 */
	public void startpre(){
		if(gCamera == null){

			  Log.e("gCamera", "进行了1次初始化");
			if(CameraUtil.openCamera()){
				gCamera = CameraUtil.camera;
			  
			  if(gCamera!=null){
				  initCamera();
			  Log.e("gCamera", "已经打开");
			  }else{
			  Log.e("gCamera", "打开失败");  
			  }
			}else{
				Log.e("gCamera", "进行过初始化，直接开始工作即可");  
			}
			  
		  }
			//可以设置参数
	}
	/**
	 * 拍照
	 */
	public void takepic(){
		Log.e("takepic", "用户要求拍照");
		gCamera.takePicture(null, null,this);	
	}
	/**
	 * 停止预览，release相机
	 */
	public void stoppre(){
		Log.e("stoppre", "用户要求取消相机使用");
		if(!CameraUtil.releaseCamera()){
			Log.e("警告", "没有关闭camera");
		};
	}
	/**
	 * 加载录像机
	 */
	public void initRecorder(){
	    Log.e("recorder", "录像机开始加载像头");
		recorder = new MediaRecorder();
		//先开预览加载相机
//		startpre();
		if(gCamera!=null){
			recorder.setCamera(gCamera);
			Log.e("录像机加载像头成功", "可以开始录制");
		}
	}
	/**
	 * 第一次开始录像时调用
	 */
	public void startRecord(SurfaceView preViewSv1){
		if (!recording) {
		    Log.e("recorder", "解锁开始");
			if(CameraUtil.unlock())
				Log.e("解锁成功", "开始设置参数");	//第一步 解锁摄像头, 否则不能录像
			//设置音频源输入
			 recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			 //设置视屏源输入
			 recorder.setVideoSource(VideoSource.CAMERA);	
			// 设置录像质量	
			 recorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_720P));
//			 recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//			 recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//			 recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
             /* ②设置錄製視頻输出格式：THREE_GPP/MPEG-4/RAW_AMR/Default
			  * THREE_GPP(3gp格式，H263视频ARM音频编码)、MPEG-4、RAW_AMR(只支持音频且音频编码要求为AMR_NB)*/
//			 recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			 /* ②设置視頻/音频文件的编码：AAC/AMR_NB/AMR_MB/Default */
//			 recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
//			 recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			 /* ②设置视频录制的分辨率。必须放在设置编码和格式的后面，否则报错 */
//			 recorder.setVideoSize(176, 144);
			/* ②设置录制的视频帧率。必须放在设置编码和格式的后面，否则报错 */
//			 recorder.setVideoFrameRate(15);                
			 /* ②设置输出文件的路径 */ 
//			 recorder.setPreviewDisplay(preViewSv1.getHolder().getSurface());	
			
			 Log.e("recorder", "参数设置完成");
			 //recorder.setOutputFile("/mnt/sdcard/" + "888bwf"+System.currentTimeMillis() + ".mp4");
			 recorder.setOutputFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "888bwf"+System.currentTimeMillis() + ".mp4");
			 try {
				recorder.prepare();
		       Log.e("recorder", "开始准备prepare");
			} catch (Exception e) {
				e.printStackTrace();
			} 
			recorder.start();   // Recording is now started
			recording = true;
		    Log.e("recorder", "开始录制。。。。");
			//开计时器			
		    timeThread = new Thread(task);
		    second = 0;
		    bool = true;
		    timeThread.start();
		}
	}
	/**
	 * 停止录像
	 */
	public void stopRecord(){
	    Log.e("recorder", "用户要求停止录像");
		if (recording){
			recorder.stop(); 
			recorder.reset();
			recorder.release();
			gCamera.lock();
		    Log.e("recorder", "停止录像,重新预览");
			recording = false;
			//停止计时
			bool = false;
		}
	}
	/**
	 * 重新开始录像时调用
	 */
	public void reRecord(){
	    Log.e("recorder", "重新录像");
		recording = true;
		 try {
			CameraUtil.unlock();
			recorder.prepare();	
			//CameraUtil.unlock();这样就不会录制
	       Log.e("recorder", "开始准备prepare");
		} catch (Exception e) {
			e.printStackTrace();
		} 
		recorder.start();   // Recording is now started
		//开计时器			
	    timeThread = new Thread(task);
	    second = 0;
	    bool = true;
	    timeThread.start();
	}
	/**
	 * 加载Camera
	 */
    public void initCamera(){
    	try {
		       if(gCamera==null){
		    	   Log.e("gCamera  neibu", "null");
		       }else{              
		    	gCamera.setPreviewCallback(this);              
				gCamera.setPreviewTexture(this);
				gCamera.startPreview();//
				CameraUtil.cameraSize = gCamera.getParameters().getPreviewSize();
//				CameraUtil.lock();
				Log.e("gCamera打开", "已经setprevieCallBack,可以开始读取数据了");
		       }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
    
    /**
     * mSurfaceTexture中继承接口于是有该回调方法
     * 回调方法取到数据源data
     */
	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		//拿到帧数据，给doSomethingNeeded处理
		mSurfaceTexture.this.doSomethingNeeded(data);
		Log.e("doSomethingNeeded打开", "data数组已经发给它");
	}
	   /**
	    * 对获取到的位图进行处理方法,简单的发出去
	    * @param bmp  实时的视屏位图
	    */
	public void doSomethingNeeded(byte[] data) {

		Message msg = new Message();
		Map<String, byte[]> map = new HashMap<String, byte[]>();
		map.put("data", data);
		//让获得消息的人能够判断是不是在录像，以便更新UI
		//拍照请忽视
		if(recording == false){
			msg.what = 0;
		}else{
			msg.what = 5;
			//把时间也发过去
			map.put("result", result.getBytes());
			Log.e("result =", result);
		}
		msg.obj = map;
		handler.sendMessage(msg);
		Log.e("handler", "data数组通过handler发给Activity了");
	}

	/**
	 * 拍照的回调方法
	 * @param data
	 * @param camera
	 */
	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		try {

			Log.e("onPictureTaken", "开始拍照，忙于存储");
			String fName = "bwf" + System.currentTimeMillis() + ".jpg";
			// 保存拍到的照片
			File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
			File file = new File(path, fName);

			FileOutputStream fos = new FileOutputStream(file);
			fos.write(data);
			fos.close();
			Log.e("onPictureTaken", "存储完成，重新预览");
			gCamera.startPreview();
			// camera.stopPreview(); //关闭预览 处理数据
			// fileScan(fName); //广播刷新一下相册
			// camera.startPreview(); // 拍照之后预览会停止, 重新开始预览

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	
//    /**
//     * 自定义的FaceTask类，开启一个线程分析数据
//     */
//    private class FaceTask extends AsyncTask<Void, Void, Void>{
// 
//        private byte[] mData;
//        //构造函数
//        public FaceTask(byte[] data) {
//        	this.mData = data;
//		}      
//        @Override
//        protected Void doInBackground(Void... params) {
//            // TODO Auto-generated method stub
//            Size size = gCamera.getParameters().getPreviewSize(); //获取预览大小
//            final int w = size.width;  //宽度
//            final int h = size.height;
//            final YuvImage image = new YuvImage(mData, ImageFormat.NV21, w, h, null);
//            ByteArrayOutputStream os = new ByteArrayOutputStream(mData.length);
//            if(!image.compressToJpeg(new Rect(0, 0, w, h), 100, os)){
//                return null;
//            }
//            byte[] tmp = os.toByteArray();
//            Bitmap bmp = BitmapFactory.decodeByteArray(tmp, 0,tmp.length); 
//            mSurfaceTexture.this.doSomethingNeeded(bmp);   //自己定义的实时分析预览帧视频的算法
//          return null;
//        }
//        
//    }
	/**
	 *  定时器设置，实现计时 
	 **/
	private int second = 0;
	private int s; //秒位
	private int m;  //分位
	private int h;  //时位
	private String result ="00:00:00";   //要显示的格式字符串
	private Handler Timehandler = new Handler();
	private boolean bool = true ;
	private Runnable task = new Runnable() {
		public void run() {
			if (bool) {
				handler.postDelayed(this, 1000);
				second ++;
				if (second < 60) {
					result = format(second);
				} else if (second < 3600) {
					m = second / 60;
					s = second % 60;
					result = format(m)+":"+format(s);
				} else {
					h = second / 3600;
					m = (second % 3600) / 60;
					s = (second % 3600) % 60;
					result =format(h)+":"+format(m)+":"+format(s);
				}
			}
		}
	};

	/** 格式化时间 */

	public String format(int i) {
		String s = i + "";
		if (s.length() == 1) {
			s = "0" + s;
		}
		return s;
	}

 

}
