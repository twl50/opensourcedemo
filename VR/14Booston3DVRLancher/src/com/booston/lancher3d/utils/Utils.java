package com.booston.lancher3d.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.booston.lancher3d.data.LanchuerItem;

public class Utils {
	public static boolean Number(String paramString) {
		for (int i = 0;; i++) {
			if (i >= paramString.length())
				return true;
			if (!Character.isDigit(paramString.charAt(i)))
				return false;
		}
	}

	public static int getInteger(int paramInt, Resources paramResources) {
		return paramResources.getInteger(paramInt);
	}

	public static void inTranslate(View paramView, boolean paramBoolean) {
		paramView.clearAnimation();
	}

	public static void installApp(Context paramContext, File paramFile) {
		Intent localIntent = new Intent();
		localIntent.setAction("android.intent.action.VIEW");
		localIntent.setDataAndType(Uri.fromFile(paramFile),
				"application/vnd.android.package-archive");
		localIntent.setFlags(268435456);
		paramContext.startActivity(localIntent);
	}

	public static boolean isContainChinese(String paramString) {
		return Pattern.compile("[һ-��]").matcher(paramString).find();
	}

	public static boolean isWifiConnected(Context paramContext) {
		return ((ConnectivityManager) paramContext
				.getSystemService("connectivity")).getNetworkInfo(1)
				.isConnected();
	}

	public static void outTranslate(View paramView, boolean paramBoolean) {
		if (paramView.getAnimation() != null)
			return;
		if (paramBoolean)
			;
		for (TranslateAnimation localTranslateAnimation = new TranslateAnimation(
				0, 0.0F, 0, 1.65F, 0, 0.0F, 0, 0.0F);; 
				localTranslateAnimation = new TranslateAnimation(
				0, 0.0F, 0, -1.65F, 0, 0.0F, 0, 0.0F)) {
			ScaleAnimation localScaleAnimation = new ScaleAnimation(1.0F,
					1.025F, 1.0F, 1.025F, 1, 0.5F, 1, 0.5F);
			AnimationSet localAnimationSet = new AnimationSet(true);
			localAnimationSet.setInterpolator(new AccelerateInterpolator());
			localAnimationSet.addAnimation(localScaleAnimation);
			localAnimationSet.addAnimation(localTranslateAnimation);
			localAnimationSet.setDuration(100L);
			localAnimationSet.setFillAfter(true);
			paramView.startAnimation(localAnimationSet);
			return;
		}
	}

	public static void sortFileName(List<LanchuerItem> paramList) {
		Collections.sort(paramList, new Comparator() {
			public int compare(LanchuerItem paramAnonymousLanchuerItem1,
					LanchuerItem paramAnonymousLanchuerItem2) {
				String[] arrayOfString = new String[2];
				arrayOfString[0] = paramAnonymousLanchuerItem1.getFile()
						.getName().toLowerCase();
				arrayOfString[1] = paramAnonymousLanchuerItem2.getFile()
						.getName().toLowerCase();
				Arrays.sort(arrayOfString);
				if ((paramAnonymousLanchuerItem1.getFile().isDirectory())
						&& (paramAnonymousLanchuerItem2.getFile().isDirectory()))
					if (!paramAnonymousLanchuerItem1.getFile().getName()
							.equalsIgnoreCase(arrayOfString[0]))
						;
				while (paramAnonymousLanchuerItem1.getFile().isDirectory()) {
					return -1;
					// if (arrayOfString[0].equalsIgnoreCase(arrayOfString[1]))
					// return 0;
					// return 1;
				}
				return 1;
			}

			@Override
			public int compare(Object arg0, Object arg1) {
				// TODO Auto-generated method stub
				return 0;
			}
		});
	}

	public static void sortFileName1(List<LanchuerItem> paramList) {
		Collections.sort(paramList, new Comparator() {
			public int compare(LanchuerItem paramAnonymousLanchuerItem1,
					LanchuerItem paramAnonymousLanchuerItem2) {
				String[] arrayOfString = new String[2];
				arrayOfString[0] = paramAnonymousLanchuerItem1.getFile()
						.getName().toLowerCase();
				arrayOfString[1] = paramAnonymousLanchuerItem2.getFile()
						.getName().toLowerCase();
				Arrays.sort(arrayOfString);
				int i;
				if (paramAnonymousLanchuerItem1.getFile().getName()
						.equalsIgnoreCase(arrayOfString[0]))
					i = -1;
				boolean bool;
				// do {
				// // return i;
				// //bool = arrayOfString[0].equalsIgnoreCase(arrayOfString[1]);
				// // i = 0;
				// } while (bool);
				// return 1;
				return 0;
			}

			@Override
			public int compare(Object arg0, Object arg1) {
				// TODO Auto-generated method stub
				return 0;
			}
		});
	}

	// public static void sortWifiByLevel(List<ScanResult> paramList) {
	// Collections.sort(paramList, new Comparator() {
	//
	// public int compare(ScanResult paramAnonymousScanResult1,
	// ScanResult paramAnonymousScanResult2) {
	// // TODO Auto-generated method stub
	// if (paramAnonymousScanResult1.level < paramAnonymousScanResult2.level)
	// return -1;
	// return 1;
	// }
	//
	// @Override
	// public int compare(Object arg0, Object arg1) {
	// // TODO Auto-generated method stub
	// return 0;
	// }
	//
	// });
	// }

	public static void startApp(Context paramContext, String paramString) {
		try {
			String str = paramContext.getPackageManager().getPackageInfo(
					paramString, 0).applicationInfo.packageName;
			Intent localIntent = paramContext.getPackageManager()
					.getLaunchIntentForPackage(str);
			localIntent.setFlags(268435456);
			paramContext.startActivity(localIntent);
			return;
		} catch (PackageManager.NameNotFoundException localNameNotFoundException) {
			localNameNotFoundException.printStackTrace();
		}
	}

	@SuppressLint({ "NewApi" })
	public static void transform(View paramView, float paramFloat1,
			float paramFloat2, float paramFloat3, int paramInt1, int paramInt2) {

		float f1;
		if (paramInt2 == 0) {
			f1 = (float) (0.11D * paramFloat1);
		} else {
			f1 = -(float) (0.11D * paramFloat1);
		}

		paramView.setTranslationX(f1);

		if (paramFloat3 > 0.0F) {

			float f2 = (float) (paramFloat3 * Math.pow(2.0D,
					paramFloat1 / 54.0F));
			float f3 = (float) (paramFloat3 * Math.pow(2.0D,
					paramFloat1 / 54.0F));

			paramView.setScaleX(f2);
			paramView.setScaleY(f3);
		}

		// paramView.setRotationY(paramFloat2);

		// paramView.setRotationX(paramFloat2);
	}

	public static void uninstallApp(Context paramContext, String paramString) {
		try {
			String str = paramContext.getPackageManager().getPackageInfo(
					paramString, 0).packageName;
			Intent localIntent = new Intent();
			localIntent.setAction("android.intent.action.DELETE");
			localIntent.setData(Uri.parse("package:" + str));
			paramContext.startActivity(localIntent);
			return;
		} catch (PackageManager.NameNotFoundException localNameNotFoundException) {
			localNameNotFoundException.printStackTrace();
		}
	}
}