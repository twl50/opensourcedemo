package com.booston.lancher3d.data;

import android.graphics.Bitmap;
import android.os.Bundle;
import java.io.File;

/*
 * 描述GridView中的每个Item
 */
public class LanchuerItem {
	public static final int TYPE_APP_STORE = 4; //应用商店
	public static final int TYPE_LOCAL_APP = 1; //全部应用
	public static final int TYPE_MEDIA = 2; //媒体
	public static final int TYPE_SETTINGS = 0; //设置
	public static final int TYPE_SIGNAL = 3; //摄像头
	
	private Bitmap bitmap;
	private File file;
	 private int left;
	private int resId;
	 private int right;
	private int selResId;
	private String title;
	private int type;

	public LanchuerItem() {
	}

	public LanchuerItem(Bundle paramBundle) {
		this.type = paramBundle.getInt("type");
		this.resId = paramBundle.getInt("res_id");
		this.title = paramBundle.getString("title");
		this.selResId = paramBundle.getInt("sel_res_id");
		this.file = new File(paramBundle.getString("path"));
	}

	public Bitmap getBitmap() {
		return this.bitmap;
	}

	public File getFile() {
		return this.file;
	}

	 public int getLeft() {
	 return this.left;
	 }

	public int getResId() {
		return this.resId;
	}

	 public int getRight() {
	 return this.right;
	 }

	public int getSelResId() {
		return this.selResId;
	}

	public String getTitle() {
		return this.title;
	}

	public int getType() {
		return this.type;
	}

	public void setBitmap(Bitmap paramBitmap) {
		this.bitmap = paramBitmap;
	}

	public void setFile(File paramFile) {
		this.file = paramFile;
	}

	 public void setLeft(int paramInt) {
	 this.left = paramInt;
	 }

	public void setResId(int paramInt) {
		this.resId = paramInt;
	}

	 public void setRight(int paramInt) {
	 this.right = paramInt;
	 }

	public void setSelResId(int paramInt) {
		this.selResId = paramInt;
	}

	public void setTitle(String paramString) {
		this.title = paramString;
	}

	public void setType(int paramInt) {
		this.type = paramInt;
	}
}