package com.booston.lancher3d.activity;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import com.booston.lancher3d.camera.CameraUtil;
import com.booston.lancher3d.camera.Record_View;
import com.booston.lancher3d.camera.mSurfaceTexture;
import com.booston.lancher3d.utils.Utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

/**
 * 操作相机功能的界面，监听各项事件。
 * 
 * @author PC
 *
 */
public class RecordActivity extends Activity implements 
OnFocusChangeListener, OnClickListener {

	private ViewGroup baseView;
	private LayoutParams params;
	private Record_View leftPart;
	private Record_View rightPart;
	private SurfaceView mRecord_sur_left1;
	private Camera camera;
	private Button rec_left;
	private TextView time_left;
	private SurfaceView mRecord_sur_right1;
	private Button rec_right;
	private TextView time_right;
	//屏幕尺寸参数
	private int screenWidth;
	private int screenHeight;
	private Rect gRect;
	// 后台处理类对象
	private mSurfaceTexture mywork;
	//camera工作状态
	private boolean camerawork = false;
	private Bitmap bmp;
	// 定义缓存状态

	// 点击控件的标签定义
	private final int REC_PIC = 0x111;
	private boolean recording = false;  //是否正在录像

	//绘图消息号
	private final int DRAWBMP = 0x444;
	//绘图handler
	private Handler drawhandler = new Handler(){
		public void handleMessage(Message msg) {
			if (msg.what == DRAWBMP ) {
				//绘图线程
				Bitmap bmp = (Bitmap) msg.obj;
				FreshPreview(bmp);
			}
		}
	};
	/**
	 * handler处理从后台返回的Bitmap集合数据
	 */
	public Handler handler = new Handler() {

		public void handleMessage(Message msg) {

			HashMap<String, byte[]> map = (HashMap<String, byte[]>) msg.obj;
			// 从mSurfaceTexture中的onPreviewFrame中data经过handler一路传过来读data
            //判断当前录制状态
			if(msg.what == 5){	
            	String strTime = new String(map.get("result"));
            	strTime = String.copyValueOf(strTime.toCharArray(), 0, map.get("result").length);
            	//统一下时间格式标准
            	String time = format(strTime);
            	//更新时间
            	time_right.setText(time);
            	time_left.setText(time);
            	Log.e("recording", "true录制中。。。。。更新了时间");
            }else if(msg.what == 0){
            	Log.e("recording", "false已经停止了录制！");
            };			
			byte[] mData = map.get("data");
			Log.e("得到数据", "mData" + mData.length);
			if (camerawork != false) {
				Size size = CameraUtil.cameraSize; // 获取预览大小
				final int w = size.width; // 宽度
				final int h = size.height;
				final YuvImage image = new YuvImage(mData, ImageFormat.NV21, w, h, null);
				ByteArrayOutputStream os = new ByteArrayOutputStream(mData.length);
				if (!image.compressToJpeg(new Rect(0, 0, w, h), 100, os)) {
					return;
				}
				byte[] tmp = os.toByteArray();
				if (tmp != null) {
					Log.e("得到数据", "tmp" + tmp.length);
				}
				// byte[] imgByte = Base64.decode(mData, Base64.DEFAULT);
				// 一直空
				bmp = BitmapFactory.decodeByteArray(tmp, 0, tmp.length);
				if (bmp != null) {
					Log.e("得到数据,bmp高度", "bmp" + bmp.getHeight());
				} else {
					Log.e("bmp为空", "");
				}
				Message bmpmsg = new Message();
				bmpmsg.what = DRAWBMP;
				bmpmsg.obj = bmp;
			    drawhandler.sendMessage(bmpmsg);
				Log.e("绘图一次结束了", "");
			}

		}
		
       /**
        * 对时间进行格式化
        * @param strTime
        * @return
        */
		private String format(String strTime) {
			String time = null;
            if (strTime.length()<=2) {
				time = "00:00:"+strTime;
			}else if(strTime.length()<=5){
				time = "00:"+strTime;
			}else{
				time = strTime;
			}
			return time;
		};
	};

	/**
	 * 构造方法
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// 去掉Activity上面的状态栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//屏幕尺寸
		DisplayMetrics dm = getResources().getDisplayMetrics();
		screenWidth = dm.widthPixels / 2;
		screenHeight = dm.heightPixels;
		gRect = new Rect(0, 0, screenWidth, screenHeight);
		//获取装左右两边内容的容器
		baseView = ((ViewGroup) View
				.inflate(this, R.layout.activity_record, null));
		params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;

		// 获取两个layout，实际上长的是一样的
		leftPart = new Record_View(this,R.layout.record_part);
		rightPart =  new Record_View(this,R.layout.record_part);
		
		// 将两个view 加到baseview上
		baseView.addView(leftPart, params);
		baseView.addView(rightPart, params);

		// 设置到activity 上并显示出来
		setContentView(baseView);

		// 控件初始化
		initView();
	}
	/**
	 * 加载控件
	 */
	protected void initView() {
		// Left part
		mRecord_sur_left1 = leftPart.getPreViewSv1();
		rec_left = leftPart.getRecordBt();

		time_left = leftPart.getTime_txt();
		
		// Right part
		mRecord_sur_right1 = rightPart.getPreViewSv1();
		rec_right = rightPart.getRecordBt();
		time_right = rightPart.getTime_txt();
		
		//监听器事件
		rec_left.setTag(REC_PIC);
		rec_left.setOnClickListener(this);
		rec_right.setTag(REC_PIC);
		rec_right.setOnClickListener(this);
		// 产生立体效果,这里有个问题，是不是连控件也要动画？
//		Utils.transform(mRecord_sur_left1, 10.0F, 0.0F, 0.0F, 0, 0);
//		Utils.transform(mRecord_sur_right1, 10.0F, 0.0F, 0.0F, 0, 1);
//		Utils.transform(rec_left, 5.0F, -5.0F, 0.0F, 1, 0);
//		Utils.transform(rec_right, 5.0F, -5.0F, 0.0F, 1, 1);
//		Utils.transform(time_left, 30.0F, 20.0F, 0.0F, 1, 0);
//		Utils.transform(time_right, 30.0F, 20.0F, 0.0F, 1, 1);
	}

	//按钮监听
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// TODO Auto-generated method stub
		int i = event.getAction();
		int j = event.getKeyCode();
		if (i == 0) {

			Log.i("sean Keycode", "Code:" + j);
			switch (j) {

			// up
			case 19:

				break;
			// down
			case 20:
				break;

			default:
				break;
			}
		}
		return super.dispatchKeyEvent(event);
	}

	/**
	 * 显示哪个surfaceview,双缓存
	 */
	private void FreshPreview(Bitmap bmp) {
		Log.e("IsfrontPre", "开始一次绘图");
		if (bmp != null)
			Log.e("bmp!=null", "");
		if (gRect != null) {
			Log.e("gRect!=null", "");
		} else {
			Log.e("gRect==null", "");
		}
		synchronized (mRecord_sur_left1.getHolder()) {
			Canvas canvas = mRecord_sur_left1.getHolder().lockCanvas();

			if (canvas != null) {
				canvas.drawBitmap(bmp, null, gRect, null);
				mRecord_sur_left1.getHolder().unlockCanvasAndPost(canvas);
				Log.e("canvas!=null", "");
			} else {
				Log.e("canvas==null", "");
			}
		}
		synchronized (mRecord_sur_right1.getHolder()) {
			Canvas canvas = mRecord_sur_right1.getHolder().lockCanvas();
			if (canvas != null) {
				canvas.drawBitmap(bmp, null, gRect, null);
				mRecord_sur_right1.getHolder().unlockCanvasAndPost(canvas);
				Log.e("canvas!=null", "");
			} else {
				Log.e("canvas==null", "");
			}
		}
	}
	
	/**
	 * 生命周期方法onStart
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// 是否照相机已经工作
		if (!camerawork) {
			Log.e("camerawork", "变成工作状态，打开预览startpre");
			// 把camera放在一个工具类里处理
			// 自定义的SurfaceTexture，可以后台读取camera的数据
			mywork = new mSurfaceTexture(10, handler);
			// 开始照相机的预览读取
			mywork.startpre();
			camerawork = true;
		}
	}
	/**
	 * 锁屏黑屏需要解开surfaceview
	 */
    @Override
    protected void onPause() {
    	super.onPause();
		mRecord_sur_left1.setVisibility(SurfaceView.GONE);
		mRecord_sur_right1.setVisibility(SurfaceView.GONE);
    }
    /**
     * 解屏需要解开surfaceview
     */
    @Override
    protected void onResume() {
    	super.onResume();
		mRecord_sur_left1.setVisibility(SurfaceView.VISIBLE);
		mRecord_sur_right1.setVisibility(SurfaceView.VISIBLE);
    }
	/**
	 * 生命周期方法onDestroy
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// 停止工作
		if (camerawork) {
			// 后台程序结束camera操作
			Log.e("camerawork", "变成非工作状态，关闭预览stoppre");
			mywork.stoppre();
			mywork.release();
			mRecord_sur_left1.setVisibility(SurfaceView.GONE);
			mRecord_sur_right1.setVisibility(SurfaceView.GONE);
			camerawork = false;
			mywork = null;
		}
	}

	private boolean isFirstRec = false;
	@Override
	public void onClick(View v) {
		// 根据控件标签Tag设别
		switch ((Integer) v.getTag()) {
		// 录像按钮
		case REC_PIC:
			if(!recording){
				Log.e("用户点击了录像按钮", "开始录像前操作");
				if (!isFirstRec) {      //第一次录
					isFirstRec = true;
					//加载录像机
					mywork.initRecorder();
					//开始录像
					mywork.startRecord(mRecord_sur_left1);
					rec_left.setText("开始录像");
					rec_right.setText("开始录像");
					recording = true;
				}else{               //不是第一次录
					mywork.reRecord();
					rec_left.setText("开始录像");
					rec_right.setText("开始录像");
					recording = true;
				}
				
			}else{
				Log.e("用户点击了停止按钮", "停止录像");
				mywork.stopRecord();
				rec_right.setText("停止录像");
				rec_left.setText("停止录像");
				recording = false;
			}
			break;
		default:
			break;
		}
	}
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		
	}
//******************************************************************************************	
	/**
	 * 各元素访问器
	 * @return
	 */
	public Button getRec_left() {
		return rec_left;
	}
	public void setRec_left(Button rec_left) {
		this.rec_left = rec_left;
	}
	public TextView getTime_left() {
		return time_left;
	}
	public void setTime_left(TextView time_left) {
		this.time_left = time_left;
	}
	public Button getRec_right() {
		return rec_right;
	}
	public void setRec_right(Button rec_right) {
		this.rec_right = rec_right;
	}
	public TextView getTime_right() {
		return time_right;
	}
	public void setTime_right(TextView time_right) {
		this.time_right = time_right;
	}

}

