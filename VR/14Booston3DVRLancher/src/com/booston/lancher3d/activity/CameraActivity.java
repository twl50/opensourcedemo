package com.booston.lancher3d.activity;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import com.booston.lancher3d.activity.R;
import com.booston.lancher3d.camera.CameraUtil;
import com.booston.lancher3d.camera.Camera_View;
import com.booston.lancher3d.camera.mSurfaceTexture;
import com.booston.lancher3d.utils.Utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * 操作相机功能的界面，监听各项事件。
 * 
 * @author PC
 *
 */
public class CameraActivity extends Activity implements OnFocusChangeListener, OnClickListener {

	private ViewGroup baseView;

	private Camera_View leftPart;

	private LinearLayout.LayoutParams params;
	private Camera_View rightPart;

	private SurfaceView mCamera_sur_left;

	private SurfaceView mCamera_sur_right;

	private ImageView pic_left;

	private Button takepic_left;

	private ImageView pic_right;

	private Button takepic_right;
	private Camera_View cam_view;

	private int screenWidth;

	private int screenHeight;

	private Rect gRect;

	private Bitmap gBitmap;
	// 左边视图的surfaceview
	private SurfaceView mCamera_sur_left1;

	// 右边surfaceview
	private SurfaceView mCamera_sur_right1;


	// 定义相机状态
	private Canvas canvas;
	private boolean camerawork = false;
	// 后台处理类对象
	private mSurfaceTexture mywork;
	public Bitmap bmp;
	// 点击控件的标签定义
	private final int TAKE_PIC = 0x111;
	private final int PIC_LEFT = 0x222;
	private final int PIC_RIGHT = 0x333;
	//绘图消息号
	private final int DRAWBMP = 0x444;
	//绘图handler
	private Handler drawhandler = new Handler(){
		public void handleMessage(Message msg) {
			if (msg.what == DRAWBMP ) {
				//绘图线程
				Bitmap bmp = (Bitmap) msg.obj;
				FreshPreview(bmp);
			}
		}
	};

	/**
	 * handler处理从后台返回的Bitmap集合数据
	 */
	public Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			// 从mSurfaceTexture中的onPreviewFrame中data经过handler一路传过来读data

			HashMap<String, byte[]> map = (HashMap<String, byte[]>) msg.obj;
			byte[] mData = map.get("data");
			Log.e("得到数据", "mData" + mData.length);
			if (camerawork != false) {
				Size size = CameraUtil.camera.getParameters().getPreviewSize(); // 获取预览大小
				final int w = size.width; // 宽度
				final int h = size.height;
				final YuvImage image = new YuvImage(mData, ImageFormat.NV21, w, h, null);
				ByteArrayOutputStream os = new ByteArrayOutputStream(mData.length);
				if (!image.compressToJpeg(new Rect(0, 0, w, h), 100, os)) {
					return;
				}
				byte[] tmp = os.toByteArray();
				if (tmp != null) {
					Log.e("得到数据", "tmp" + tmp.length);
				}
				// byte[] imgByte = Base64.decode(mData, Base64.DEFAULT);
				// 一直空
				bmp = BitmapFactory.decodeByteArray(tmp, 0, tmp.length);
				if (bmp != null) {
					Log.e("得到数据,bmp高度", "bmp" + bmp.getHeight());
				} else {
					Log.e("bmp为空", "");
				}
				Message bmpmsg = new Message();
				bmpmsg.what = DRAWBMP;
				bmpmsg.obj = bmp;
			    drawhandler.sendMessage(bmpmsg);
				// 绘图并显示,开子线程绘制
				// new Thread(new Runnable() {
				// public void run() {
//！！！！！！##&&***&&这里画图，出错！！！
				//FreshPreview(bmp);
				Log.e("绘图一次结束了", "");
				// }
				// }).start();

			}

		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Log.e("onCreate", "oncreate主Activity");
		// 去掉Activity上面的状态栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		DisplayMetrics dm = getResources().getDisplayMetrics();
		screenWidth = dm.widthPixels / 2;
		screenHeight = dm.heightPixels;
		gRect = new Rect(0, 0, screenWidth, screenHeight);

		// 获取装左右两边内容的容器
		baseView = ((ViewGroup) View.inflate(this, R.layout.activity_camera, null));
		params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;

		// 获取两个layout，实际上长的是一样的
		leftPart = new Camera_View(this, R.layout.camera_part, gRect);
		rightPart = new Camera_View(this, R.layout.camera_part, gRect);

		// 将两个view 加到baseview上
		baseView.addView(leftPart, params);
		baseView.addView(rightPart, params);

		// 设置到activity 上并显示出来
		setContentView(baseView);

		// 控件初始化
		initView();

	}

	protected void initView() {

		// Left part
		mCamera_sur_left1 = leftPart.getPreviewSV1();
		pic_left = leftPart.getPicturePreview();
		pic_left.setTag(PIC_LEFT);
		takepic_left = leftPart.getTakepicture();
		takepic_left.setTag(TAKE_PIC);

		// Right part
		mCamera_sur_right1 = rightPart.getPreviewSV1();
		pic_right = rightPart.getPicturePreview();
		pic_right.setTag(PIC_RIGHT);
		takepic_right = rightPart.getTakepicture();
		takepic_right.setTag(TAKE_PIC);

		// 给缩略图和按钮添加监听器
		pic_left.setOnClickListener(this);
		takepic_left.setOnClickListener(this);
		pic_right.setOnClickListener(this);
		takepic_right.setOnClickListener(this);
		Log.e("initView", "主Activity控件初始化完成");
		// // 产生立体效果,这里有个问题，是不是连控件也要动画？
		// Utils.transform(mCamera_sur_left, 10.0F, 0.0F, 0.0F, 0, 0);
		// Utils.transform(mCamera_sur_right, 10.0F, 0.0F, 0.0F, 0, 1);
		// Utils.transform(pic_left, 5.0F, -5.0F, 0.0F, 1, 0);
		// Utils.transform(pic_right, 5.0F, -5.0F, 0.0F, 1, 1);
		// Utils.transform(takepic_left, 30.0F, 20.0F, 0.0F, 1, 0);
		// Utils.transform(takepic_right, 30.0F, 20.0F, 0.0F, 1, 1);
	}

	/**
	 * 生命周期方法onStart
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// 是否照相机已经工作
		if (!camerawork) {
			Log.e("camerawork", "变成工作状态，打开预览startpre");
			// 把camera放在一个工具类里处理
			// 自定义的SurfaceTexture，可以后台读取camera的数据
			mywork = new mSurfaceTexture(10, handler);
			// 开始照相机的预览读取
			mywork.startpre();
			camerawork = true;
		}
	}

	/**
	 * 生命周期方法onDestroy
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// 停止工作
		if (camerawork) {
			// 后台程序结束camera操作
			Log.e("camerawork", "变成非工作状态，关闭预览stoppre");
			mywork.stoppre();
			mCamera_sur_left1.setVisibility(SurfaceView.GONE);
			mCamera_sur_right1.setVisibility(SurfaceView.GONE);
			camerawork = false;
			mywork = null;
		}
	}

	/**
	 * 显示哪个surfaceview,双缓存
	 */
	private void FreshPreview(Bitmap bmp) {
		Log.e("IsfrontPre", "开始一次绘图");
		if (bmp != null)
			Log.e("bmp!=null", "");
		if (gRect != null) {
			Log.e("gRect!=null", "");
		} else {
			Log.e("gRect==null", "");
		}
		synchronized (mCamera_sur_left1.getHolder()) {
			Canvas canvas = mCamera_sur_left1.getHolder().lockCanvas();

			if (canvas != null) {
				canvas.drawBitmap(bmp, null, gRect, null);
				mCamera_sur_left1.getHolder().unlockCanvasAndPost(canvas);
				Log.e("canvas!=null", "");
			} else {
				Log.e("canvas==null", "");
			}
		}

		synchronized (mCamera_sur_right1.getHolder()) {
			Canvas canvas = mCamera_sur_right1.getHolder().lockCanvas();
			if (canvas != null) {
				canvas.drawBitmap(bmp, null, gRect, null);
				mCamera_sur_right1.getHolder().unlockCanvasAndPost(canvas);
				Log.e("canvas!=null", "");
			} else {
				Log.e("canvas==null", "");
			}
		}

	}

	// 按钮监听
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// TODO Auto-generated method stub
		int i = event.getAction();
		int j = event.getKeyCode();
		if (i == 0) {

			Log.i("sean Keycode", "Code:" + j);
			switch (j) {

			// up
			case 19:

				break;
			// down
			case 20:
				break;

			default:
				break;
			}
		}
		return super.dispatchKeyEvent(event);
	}

	/**
	 * 各组件点击事件
	 */
	@Override
	public void onClick(View v) {
		// 根据控件标签Tag设别
		switch ((Integer) v.getTag()) {
		// 拍照按钮
		case TAKE_PIC:
			mywork.takepic();
			break;
		// 右边缩略图
		case PIC_RIGHT:
			// 可以是打开查看大图或者相册之类功能
			break;
		// 左边缩略图
		case PIC_LEFT:

			break;
		default:
			break;
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub

	}

}
