package com.booston.lancher3d.activity;

import java.util.ArrayList;
import java.util.List;

import com.booston.lancher3d.adapter.LanchuerAdapter;
import com.booston.lancher3d.data.LanchuerItem;
import com.booston.lancher3d.utils.Utils;
import com.booston.lancher3d.activity.R;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements 
		OnFocusChangeListener, OnClickListener {

	private ViewGroup baseView;
	 
	private LinearLayout l_info;
	private TextView l_brand;
	 
	private TextView l_title;
	private LinearLayout l_view;
	private LinearLayout leftPart;

	private LanchuerAdapter mAdapter;
	private LanchuerAdapter mRightAdapter;

	private GridView mGridView_left;
	private GridView mGridView_right;

	private List<LanchuerItem> mList = new ArrayList();

	private LinearLayout.LayoutParams params;
	private LinearLayout rightPart;
	private LinearLayout rl_info;
	private TextView rl_brand;
	 
	private TextView rl_title;
	private LinearLayout rl_view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// 去掉Activity上面的状态栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		//获取装左右两边内容的容器
		baseView = ((ViewGroup) View
				.inflate(this, R.layout.activity_base, null));
		params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;

		// 获取两个layout，实际上长的是一样的
		leftPart = ((LinearLayout) View.inflate(this, R.layout.launcher_main,
				null));
		rightPart = ((LinearLayout) View.inflate(this, R.layout.launcher_main,
				null));
		
		// 将两个view 加到baseview上
		baseView.addView(leftPart, params);
		baseView.addView(rightPart, params);

		// 设置到activity 上并显示出来
		setContentView(baseView);

		// 控件初始化
		initView();

		// Item初始化
		initItem();
	}

	protected void initView() {
		// Left part
		mGridView_left = ((GridView) this.leftPart.findViewById(R.id.gridview));

		l_title = ((TextView) leftPart.findViewById(R.id.l_title));

		l_info = ((LinearLayout) leftPart.findViewById(R.id.l_info));
		l_view = ((LinearLayout) leftPart.findViewById(R.id.l_view));

		l_brand = ((TextView) leftPart.findViewById(R.id.brand));

		// Right part
		mGridView_right = ((GridView) rightPart.findViewById(R.id.gridview));
	
		rl_title = ((TextView) rightPart.findViewById(R.id.l_title));
		rl_info = ((LinearLayout) rightPart.findViewById(R.id.l_info));
		rl_view = ((LinearLayout) rightPart.findViewById(R.id.l_view));
		rl_brand = ((TextView) rightPart.findViewById(R.id.brand));

		l_title.setVisibility(View.GONE);
		rl_title.setVisibility(View.GONE);

		// 产生立体效果
		Utils.transform(l_view, 10.0F, 0.0F, 0.0F, 0, 0);
		Utils.transform(rl_view, 10.0F, 0.0F, 0.0F, 0, 1);
		Utils.transform(l_title, 5.0F, -5.0F, 0.0F, 1, 0);
		Utils.transform(rl_title, 5.0F, -5.0F, 0.0F, 1, 1);
		Utils.transform(l_info, 30.0F, 20.0F, 0.0F, 1, 0);
		Utils.transform(rl_info, 30.0F, 20.0F, 0.0F, 1, 1);
		Utils.transform(l_brand, 30.0F, 20.0F, 0.0F, 1, 0);
		Utils.transform(rl_brand, 30.0F, 20.0F, 0.0F, 1, 1);
	}

	// 处理每个Item被点击
	public void handleClick(LanchuerItem paramLanchuerItem) {

		switch (paramLanchuerItem.getType()) {
		case 0:
			Toast.makeText(getApplicationContext(), "设置！", Toast.LENGTH_SHORT)
					.show();
			break;
		case 1:
			Toast.makeText(getApplicationContext(), "全部应用！", Toast.LENGTH_SHORT)
					.show();
			break;
		case 2:
			Toast.makeText(getApplicationContext(), "媒体！", Toast.LENGTH_SHORT)
					.show();
			Intent record = new Intent(this,RecordActivity.class);
			startActivity(record);
			break;
		case 3:
			Toast.makeText(getApplicationContext(), "相机！", Toast.LENGTH_SHORT)
					.show();
			Intent takepic = new Intent(this,CameraActivity.class);
			startActivity(takepic);
			break;
		case 4:
			Toast.makeText(getApplicationContext(), "应用商店！", Toast.LENGTH_SHORT)
					.show();
			break;
		default:
			break;
		}
	}

	//控件往左
	private void goLeft() {
		int k;
		if (mAdapter.getSelected() != 0) {
			k = mAdapter.getSelected() - 1;
		} else {
			k = 4;
		}

		mAdapter.setSelected(k);
		mRightAdapter.setSelected(k);
		mAdapter.notifyDataSetChanged();
		mRightAdapter.notifyDataSetChanged();
	}

	//控件往右
	private void goRight() {
		int k;
		if (mAdapter.getSelected() != 4) {
			k = mRightAdapter.getSelected() + 1;
		} else {
			k = 0;
		}
		mAdapter.setSelected(k);
		mRightAdapter.setSelected(k);
		mAdapter.notifyDataSetChanged();
		mRightAdapter.notifyDataSetChanged();
	}
 
	//按钮监听
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// TODO Auto-generated method stub
		int i = event.getAction();
		int j = event.getKeyCode();
		if (i == 0) {

			Log.i("sean Keycode", "Code:" + j);
			switch (j) {

			// up
			case 19:

				break;
			// down
			case 20:
				break;
				
			// left
			case 21:
				goLeft();
				break;
				
			// right
			case 22:
				goRight();
				break;
			//OK
			case 23:
				handleClick((LanchuerItem)mList.get(mAdapter
						.getSelected()));
				break;
			case 24:

				break;
			case 25:

				break;
			default:
				break;
			}
		}
		return super.dispatchKeyEvent(event);
	}

	// luncher 中GridLayout中装的Item，即每个功能图标，如若需后期增加只需多new一个item
	public void initItem() {
		new LanchuerItem();
		LanchuerItem localLanchuerItem1 = new LanchuerItem();
		localLanchuerItem1.setResId(R.drawable.settings);
		localLanchuerItem1.setTitle("设置");
		localLanchuerItem1.setType(0);
		localLanchuerItem1.setSelResId(R.drawable.settings_selected);
		this.mList.add(localLanchuerItem1);

		LanchuerItem localLanchuerItem2 = new LanchuerItem();
		localLanchuerItem2.setResId(R.drawable.mediaplayer);
		localLanchuerItem2.setTitle("视频");
		localLanchuerItem2.setSelResId(R.drawable.mediaplayer_selected);
		localLanchuerItem2.setType(2);
		this.mList.add(localLanchuerItem2);

		LanchuerItem localLanchuerItem3 = new LanchuerItem();
		localLanchuerItem3.setResId(R.drawable.appstore);
		localLanchuerItem3.setTitle("应用商店");
		localLanchuerItem3.setSelResId(R.drawable.appstore_selected);
		localLanchuerItem3.setType(4);
		this.mList.add(localLanchuerItem3);

		LanchuerItem localLanchuerItem4 = new LanchuerItem();
		localLanchuerItem4.setResId(R.drawable.camera);
		localLanchuerItem4.setSelResId(R.drawable.camera_selected);
		localLanchuerItem4.setTitle("相机");
		localLanchuerItem4.setType(3);
		this.mList.add(localLanchuerItem4);

		// 初始化 item 各个参数
		LanchuerItem localLanchuerItem5 = new LanchuerItem();
		localLanchuerItem5.setResId(R.drawable.appcenter);
		localLanchuerItem5.setTitle("全部应用");
		localLanchuerItem5.setSelResId(R.drawable.appcenter_selected);
		localLanchuerItem5.setType(1);

		// 添加到表里面
		mList.add(localLanchuerItem5);

		// new 适配器
		this.mAdapter = new LanchuerAdapter(this, this.mList, false);
		this.mRightAdapter = new LanchuerAdapter(this, this.mList, true);

		// 给每个功能块设置 适配器
		this.mGridView_left.setAdapter(this.mAdapter);
		this.mGridView_right.setAdapter(this.mRightAdapter);

		// 设置选择
		this.mRightAdapter.setSelected(2);
		this.mAdapter.setSelected(2);

		// 刷新 提交变更
		this.mAdapter.notifyDataSetChanged();
		this.mRightAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFocusChange(View arg0, boolean arg1) {
		// TODO Auto-generated method stub
		
	}

}
