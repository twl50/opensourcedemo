package com.booston.lancher3d.adapter;

import java.util.ArrayList;
import java.util.List;

import com.booston.lancher3d.activity.MainActivity;
import com.booston.lancher3d.data.LanchuerItem;
import com.booston.lancher3d.utils.Utils;
import com.booston.lancher3d.activity.R;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LanchuerAdapter extends LanchuerBaseAdapter {

	float[] degrees = { 25.0F, 20.0F, 0.0F, -20.0F, -25.0F };
	private LayoutInflater inflater;
	private Context mContext;
	private List<LanchuerItem> mList = new ArrayList();
	private DisplayMetrics metrics;
	private BitmapFactory.Options opts;

	public LanchuerAdapter(Context paramContext, List<LanchuerItem> paramList,
			boolean paramBoolean) {
		mContext = paramContext;
		mList = paramList;
		inflater = LayoutInflater.from(paramContext);
		opts = new BitmapFactory.Options();
		metrics = mContext.getResources().getDisplayMetrics();
		opts.inPurgeable = true;
		opts.inInputShareable = true;
		setRight(paramBoolean);
	}

	public int getCount() {
		return mList.size();
	}

	public Object getItem(int paramInt) {
		return mList.get(paramInt);
	}

	public long getItemId(int paramInt) {
		return paramInt;
	}

	@Override
	public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {

		int i = 1;
		final LanchuerItem localLanchuerItem = (LanchuerItem) mList
				.get(paramInt);

		// item
		if (paramView == null)
			paramView = this.inflater.inflate(R.layout.gridview_item,
					paramViewGroup, false);

		// 图标贴图
		ImageView localImageView = (ImageView) paramView
				.findViewById(R.id.icon_view);
		localImageView.setImageResource(localLanchuerItem.getResId());

		// 标题
		TextView localTextView = (TextView) paramView.findViewById(R.id.title);
		localTextView.setText(localLanchuerItem.getTitle());

		// item
		LinearLayout localLinearLayout = (LinearLayout) paramView
				.findViewById(R.id.main_item);

		//设置点击监听器
		paramView.setOnClickListener(new View.OnClickListener() {
			public void onClick(View paramAnonymousView) {
				if ((LanchuerAdapter.this.mContext instanceof MainActivity))
					((MainActivity) LanchuerAdapter.this.mContext)
							.handleClick(localLanchuerItem);
			}
		});

		//长按监听器
		paramView.setOnLongClickListener(new View.OnLongClickListener() {
			public boolean onLongClick(View paramAnonymousView) {
				return false;
			}
		});

		// 给获取焦点的控件设置动画效果并产生立体效果
		if (this.currentFocusId == paramInt) {
			localLinearLayout.bringToFront();
			localLinearLayout.startAnimation(this.bounceOutAnim);
			localImageView.setImageResource(localLanchuerItem.getSelResId());
			localTextView.setVisibility(0);

			float f3 = 50 + 5 * Math.abs(2 - paramInt);
			float f4 = this.degrees[paramInt];
			if (this.isRight) {
				i = 1;
			} else {
				i = 0;
			}
			Utils.transform(paramView, f3, f4, 0.5F, 0, i);
			return paramView;
		}

		if (localLinearLayout.getAnimation() != null) {
			localLinearLayout.startAnimation(this.bounceInAnim);
			localLinearLayout.setAnimation(null);
		}
		localImageView.setImageResource(localLanchuerItem.getResId());
		localTextView.setVisibility(4);
		float f1 = 5 * Math.abs(2 - paramInt);
		float f2 = this.degrees[paramInt];
			if (this.isRight) {
			i = 1;
		} else {
			i = 0;
		}

		Utils.transform(paramView, f1, f2, 1.0F, 0, i);

		return paramView;
	}
}
