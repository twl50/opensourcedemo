package com.booston.lancher3d.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;

public class LanchuerBaseAdapter extends BaseAdapter {
	protected static final int DURATION = 400;
	protected Animation bounceInAnim = null;
	protected Animation bounceOutAnim = null;
	protected int currentFocusId = 2;
	protected boolean isRight = false;

	public LanchuerBaseAdapter() {
		createAnimation();
	}

	private void createAnimation() {
		if (bounceOutAnim == null)
			bounceOutAnim = createScaleAnimation(1.0F, 1.1F, 1.0F, 1.1F);
		if (bounceInAnim == null)
			bounceInAnim = createScaleAnimation(1.1F, 1.0F, 1.1F, 1.0F);
	}

	private Animation createScaleAnimation(float paramFloat1,
			float paramFloat2, float paramFloat3, float paramFloat4) {
		ScaleAnimation localScaleAnimation = new ScaleAnimation(paramFloat1,
				paramFloat2, paramFloat3, paramFloat4, 1, 0.5F, 1, 0.5F);
		localScaleAnimation.setFillAfter(true);
		localScaleAnimation.setInterpolator(new AccelerateInterpolator());
		localScaleAnimation.setDuration(200L);
		return localScaleAnimation;
	}

	public int getCount() {
		return 0;
	}

	public Object getItem(int paramInt) {
		return null;
	}

	public long getItemId(int paramInt) {
		return 0L;
	}

	public int getSelected() {
		return this.currentFocusId;
	}

	public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
		return null;
	}

	public boolean isRight() {
		return this.isRight;
	}

	public void setRight(boolean paramBoolean) {
		this.isRight = paramBoolean;
	}

	public void setSelected(int paramInt) {
		this.currentFocusId = paramInt;
	}
}